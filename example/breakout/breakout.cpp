#include "breakout.hpp"

namespace breakout {

enum class projectile { ball, laser, machine_gun };

breakout::breakout(const std::string& level)
{
   std::ifstream in{ level };
   block_type b;
   in >> _xmax;
   while (in.eof() == false)
   {
      in >> b;
      std::string path;
      switch (b)
      {
      case block::empty:
         break;
      case block::immutable:
         path = "tex/immutable.png";
         break;
      case block::normal:
         path = "tex/normal.png";
         break;
      case block::magnetic:
         path = "tex/magnetic.png";
         break;
      case block_type::laser:
         path = "tex/laser.png";
         break;
      case block::machine_gun:
         path = "tex/machine_gun.png";
         break;
      case block::explosive:
         path = "tex/explosive.png";
         break;
      default:
         throw std::logic_error{ "Cannot determine block type." };
      }

      _blocks.emplace_back(std::make_unique<block>(path, x, y, b));
      x += block::size;
      if (x == _xmax)
      {
         x = 0;
         ++y;
      }
   }

   _blocks.emplace_back(std::make_unique<doge::paddle>("tex/paddle.png", _xmax / 2, _ymax));
   _blocks.emplace_back(std::make_unique<doge::ball>("tex/ball.png", _xmax / 2, _ymax - 20));

   _sound.load(sound::empty);
}

void breakout::loop()
{
   if (_menu)
      menu();
   else
   {
      doge::keyboard::on_key_down('A',
      [this]()
      {
         if (_paddle.x > _paddle.size)
            _paddle.move(-1.0f);
      });
      doge::keyboard::on_key_down('L',
      [this]()
      {
         if (_paddle.x < _xmax - _paddle.size)
            _paddle.move(+1.0f);
      });
      doge::keyboard::on_key_press(GLFW_SPACE,
      [this]()
      {
         if (_projectile == projectile::ball)
            fire_ball();
         else if (_projectile == projectile::laser)
            fire_laser();
         else if (_projectile == projectile::machine_gun)
            fire_machine_gun();
         else
            _sound.play(sound::empty);
      });
      doge::keyboard::on_key_press(GLFW_ESCAPE, [this](){ _menu = true; });

      for (auto it = _blocks.begin(); it != _blocks.end() - 2; ++it)
      {
         auto b = std::dynamic_pointer_cast<block>(*it);
         switch (b->block_type())
         {
         case block::immutable:
            break;
         case block::normal:
            break;
         case block::magnetic:
            break;
         case block::laser:
            break;
         case block::machine_gun:
            break;
         case block::explosive:
            break;
         default:
         }
      }

      for (auto cit = _blocks.cbegin(); cit != _blocks.cend(); ++cit)
         cit->draw();
   }
}

void breakout::main_menu()
{
   doge::on_key_press(GLFW_ESCAPE, [this](){ _menu = false; });
}
}