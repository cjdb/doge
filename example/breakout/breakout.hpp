#ifndef DOGE_BREAKOUT_EXAMPLE_HPP14_INCLUDED
#define DOGE_BREAKOUT_EXAMPLE_HPP14_INCLUDED
#include <doge/entity/entity.hpp>
#include <doge/game.hpp>
namespace breakout {
class breakout : public game
{
   enum class block_type { empty, immutable1, immutable2, immutable3, normal1, normal2, normal3,
                           special1, special2, special3 };
   enum class sound { empty, collision };
   std::list<doge::shared_entity> _blocks;
   std::uint32_t _xmax;
   bool _load;

   void main_menu();
public:
   breakout(const std::string& level);
   void loop();
};
}
#endif // DOGE_BREAKOUT_EXAMPLE_HPP14_INCLUDED
