/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Defines the memory class used in doge
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cassert>
#include <cstdint>
#include <doge/system/error_codes.hpp>
#include <doge/system/memory.hpp>
#include <doge/system/memory_list.hpp>
#include <doge/utility/power_of_two.hpp>
#include <doge/utility/units.hpp>

namespace {
void* head;
doge::system::memory_list free_list;
}

namespace doge {
namespace system
{
std::size_t memory::_available = 0;
std::size_t memory::_total_allocation = 0;

memory::memory(const std::size_t size)
{
   // ensure that we only have one memory class instantiated
   assert(head == nullptr);

   // memory size must be a power of two
   // if memory size isn't a power of two, we have a logical error
   // can't throw std::logic_error, since that might use new/new[]
   // will need to revert to the C way of using error codes for this class
   if (power_of_two(size) == false)
   {
      throw error_code::bad_allocation_size;
   }
   else
   {
      free_list.head_ = reinterpret_cast<node*>(std::malloc(size));
      head = free_list.head_;
      _available = size;
      _total_allocation = size;
      
      // check that the memory has been allocated
      if (head)
      {
         free_list.head_->next = free_list.head_;
         free_list.head_->size = size;
      }
      else
      {
         throw error_code::insufficient_memory;
      }
   }
}

memory::~memory()
{
   if (head)
   {
      std::free(head);
      head = nullptr;
      free_list.head_ = nullptr;
   }
}

void* memory::malloc(std::size_t size) noexcept
{
   // make sure that every byte starts at an address of 4
   // we can do this by making sure every size is divisible by 4
   constexpr std::size_t alignment = 4;
   size += sizeof(node);
   auto adjustment = size % alignment;

   if (adjustment)
      size += (alignment - adjustment);

   auto* region = free_list.find(size);
   if (region)
   {
      free_list.remove(region, size);
      auto* raw = reinterpret_cast<char*>(region);
      raw += sizeof(node);
      return raw;
   }
   else
   {
      return nullptr;
   }
}

error_code memory::free(void* p) noexcept
{
   auto* raw = reinterpret_cast<char*>(p);
   const auto* raw_head = reinterpret_cast<const char*>(head);
   if (raw == nullptr                  ||
       raw < (raw_head + sizeof(node)) || raw >= (raw_head + _total_allocation - sizeof(node)))
      return error_code::memory_out_of_bounds;

   raw -= sizeof(node);
   auto* region = reinterpret_cast<node*>(raw);

   free_list.insert(region);
   return error_code::no_error;
}

#ifndef NDEBUG
const memory_list& get_list()
{
   return free_list;
}
#endif // NDEBUG
}
}
