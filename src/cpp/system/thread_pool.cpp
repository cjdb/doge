/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/12/23
 *
 * Provides the implementation for the thread pool class.
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <algorithm>
#include <doge/system/thread_pool.hpp>
#include <stdexcept>

#ifndef NDEBUG
#include <iostream>
#endif // NDEBUG

#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

namespace {
std::mutex debugger;
}

namespace doge {
namespace system {
thread_pool::thread_pool() : _thread_count{ std::thread::hardware_concurrency() - 1 },
                             _finished{ false },
                             _demons(_thread_count / 2)
{
   try
   {
      for (std::size_t i = 0; i < _thread_count; ++i)
         _pool.emplace_back(&thread_pool::task_manager, this);
   }
   catch (...)
   {
      _finished = true;
      throw std::logic_error{ "Unable to set up thread pool" };
   }
}

thread_pool::~thread_pool()
{
   _finished = true;
   for (auto it = _pool.begin(); it != _pool.end(); ++it)
      if (it->joinable())
         it->join();
}

void thread_pool::task_manager()
{
#ifndef NDEBUG
   static std::size_t thread_counter = 0;
   std::size_t thread_id = 0;
   {
      std::lock_guard<std::mutex> lock{ debugger };
      thread_id = thread_counter++;
   }
   std::size_t accumulator = 0;
#endif // NDEBUG
   try
   {
      while (_finished == false)
      {
         std::shared_ptr<base_task> t{ nullptr };
         if (_tasks.try_pop(t))
         {
#ifndef NDEBUG
            ++accumulator;
#endif // NEBUG
            t->execute();
         }
         else
         {
            auto disowned = std::find_if(_demons.begin(), _demons.end(), [](const auto& d){ return d.disowned(); });
            if (disowned == _demons.cend())
               std::this_thread::yield();
            else
               disowned->repossess();
         }
      }
   }
   catch (...)
   {
      _finished = true;
   }
#ifndef NDEBUG
   std::lock_guard<std::mutex> lock{ debugger };
   std::clog << "End of thread " << thread_id << ": " << accumulator << std::endl;
#endif // NDEBUG
}

std::size_t thread_pool::possess_thread() noexcept
{
   auto available = std::find_if(_demons.begin(), _demons.end(), [](const auto& d){ return d.available(); });

   if (available == _demons.end())
      return 0;

   available->occupy();
   auto& ref = *available;
   auto f = std::bind(&demon::possess, &ref);
   add_task(f);
   return available - _demons.cbegin();
}

bool thread_pool::kill_demon(const std::size_t id) noexcept
{
   if (id < _demons.size())
      return _demons[id].kill();
   else
      return false;
}

bool thread_pool::disown_demon(const std::size_t id) noexcept
{
   if (id < _demons.size())
      return _demons[id].disown();
   else
      return false;
}

thread_pool::demon::demon() : _signal{ signal::available }
{
}

thread_pool::demon::~demon()
{
   _signal = signal::destroy;
}

void thread_pool::demon::possess()
{
   while (_signal == signal::possessing)
   {
      try
      {
         std::shared_ptr<base_task> t{ nullptr };
         if (_tasks.empty() == false)
         {
            t = _tasks.front();
            _tasks.pop();
            t->execute();
         }
         else
         {
            std::this_thread::yield();
         }
      }
      catch (...)
      {
         _signal = signal::killed;
      }
   }

   _disowned_tasks.move(std::move(_tasks));

   while (_signal == signal::disowned)
   {
      repossess();
   }

   decltype(_tasks) empty;
   std::swap(empty, _tasks);
   _disowned_tasks.clear();
   if (_signal != signal::destroy)
      _signal = signal::available;
}

inline void thread_pool::demon::repossess()
{
   try
   {
      std::shared_ptr<base_task> t{ nullptr };
      if (_disowned_tasks.try_pop(t))
         t->execute();
      else
         std::this_thread::yield();
   }
   catch (...)
   {
      _signal = signal::killed;
   }
}
}
}
