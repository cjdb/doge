/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Defines memory usage using new/delete
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <doge/system/memory.hpp>
#include <stdexcept>

void* operator new(const std::size_t size)
{
   void* p = doge::system::memory::malloc(size);
   if (p == nullptr)
      throw std::bad_alloc{ };
   return p;
}

void* operator new(const std::size_t size, const std::nothrow_t&) noexcept
{
   return doge::system::memory::malloc(size);
}

void operator delete(void* p) noexcept
{
   if (p)
      doge::system::memory::free(p);
}

void operator delete(void*, std::size_t) noexcept { }
