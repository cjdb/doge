/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Defines the game engine class that defines doge
 * There must be exactly one game engine class in a program.
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <doge/engine.hpp>
//#include <doge/utility/log.hpp>
#include <iostream>

namespace pt = boost::property_tree;
namespace doge {
engine::engine(const std::vector<std::string>&, bool keyboard_init, bool mouse_init, int major, int minor)
   : _screen{ 1024, 768, 4, nullptr, nullptr, nullptr, false, false }
{
   if (glfwInit())
   {
      _screen.monitor = glfwGetPrimaryMonitor();
      _screen.vidmode = glfwGetVideoMode(_screen.monitor);
      read_config();

      glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
      glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
      glfwWindowHint(GLFW_SAMPLES, _screen.antialiasing);

      create_window();

      const auto* renderer = gl::GetString(gl::RENDERER);
      const auto* version = gl::GetString(gl::VERSION);
      std::clog << "Renderer: " << renderer << '\n';
      std::clog << "Version: " << version << '\n';

      if (keyboard_init)
      {
         std::clog << "Initialising keyboard...\n";
         hid::keyboard::init(_screen.window);
      }

      if (mouse_init)
      {
         std::clog << "Initialising mouse...\n";
         hid::mouse::init(_screen.window);
      }
   }
   else
   {
      throw std::runtime_error{ "Failed to initialise glfw3." };
   }
}

engine::~engine()
{
   if (_screen.window)
      glfwDestroyWindow(_screen.window);
   glfwTerminate();
}

void engine::run()
{
   while (!glfwWindowShouldClose(_screen.window))
   {
      gl::ClearColor(0.35f, 0.35f, 0.35f, 1.0f);
      gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
      gl::DepthFunc(gl::LESS);
      glfwPollEvents();
      gl::Viewport(0, 0, _screen.width, _screen.height);
      _game->frame();
      _game->draw();
      glfwSwapBuffers(_screen.window);
   }
}

void engine::read_config()
{
   pt::ptree ini_vars;
   pt::ini_parser::read_ini("config.ini", ini_vars);

   auto width = ini_vars.get(cvars::screen_width, _screen.vidmode->width);
   auto height = ini_vars.get(cvars::screen_height, _screen.vidmode->height);

   _screen.width = (width < _screen.vidmode->width) ? width : _screen.vidmode->width;
   _screen.height = (height < _screen.vidmode->height) ? height : _screen.vidmode->height;
   _screen.fullscreen = ini_vars.get(cvars::fullscreen, true);
   _screen.vsync = ini_vars.get(cvars::vsync, false);
   _screen.antialiasing = ini_vars.get(cvars::antialiasing, 0);
}

void engine::create_window()
{
   if (_screen.window)
      glfwDestroyWindow(_screen.window);
   _screen.window = glfwCreateWindow(_screen.width,
                                     _screen.height,
                                     "doge",
                                     _screen.fullscreen ? _screen.monitor : nullptr,
                                     nullptr);
   auto gl_load_gen = gl::sys::LoadFunctions();
   if (_screen.window && gl_load_gen)
      glfwMakeContextCurrent(_screen.window);
   else
      throw std::runtime_error{ "Could not open window with glfw3." };
}
}
