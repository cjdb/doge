/*#include <GL/glew.h>
#include <cmath>
#include <doge/engine.hpp>
#include <doge/entity/camera.hpp>
#include <doge/utility/math.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <iostream>

namespace doge {

using math::rad::versor;

namespace entity {
const glm::vec4 camera::unit_forward{ 0.0f, 0.0f, -1.0f, 0.0f };
const glm::vec4 camera::unit_right{ 1.0f, 0.0f,  0.0f, 0.0f };
const glm::vec4 camera::unit_up{ 0.0f, 1.0f,  0.0f, 0.0f };
screen_data camera::_screen_data;

camera::camera(const glm::vec4& position,
               const std::vector<std::uint32_t>& shaders,
               const float aspect_ratio,
               const std::array<std::string, 2> shader_var,
               const float field_of_view,
               const glm::vec4& forward,
               const glm::vec4& right,
               const glm::vec4& up,
               const glm::vec4& rotation,
               const glm::vec4& translation_speed,
               const glm::vec4& rotation_speed,
               const std::array<float, 2>& clip)
   : proj{ proj_ },
     view{ view_ },
     position{ position_ },
     forward_{ forward },
     right_{ right },
     up_{ up },
     position_{ position },
     rotation_{ rotation },
     clipping_{ clip },
     field_of_view_{ field_of_view },
     aspect_{ aspect_ratio },
     range_{ std::tan(field_of_view_ / 2.0f) * clipping_[near] },
     shaders_{ shaders },
     updated_{ false }
{
   //quat_ = { 0.0f, 0.0f, 0.0f, 5.0f };
   speed_[doge::entity::speed::translation] = translation_speed;
   speed_[doge::entity::speed::rotation] = rotation_speed;

   proj_[0][0] = (2.0f * clipping_[near]) / (range_ * aspect_ * 2.0f);
   proj_[1][1] = clipping_[near] / range_;
   proj_[2][2] = -(clipping_[far] + clipping_[near]) / (clipping_[far] - clipping_[near]);
   proj_[3][2] = -(2.0f * clipping_[far] * clipping_[near]) / (clipping_[far] - clipping_[near]);
   proj_[2][3] = -1.0f;
   proj_[3][3] = 0.0f;

   auto t = math::translate(-position_);
   quat_ = versor(-rotation_.y, { 0.0f, 1.0f, 0.0f, 0.0f });
   glm::highp_mat4 r = glm::mat4_cast(quat_);//versor(rotation_.y, { 0.0f, 1.0f, 0.0f, 0.0f }));
   view_ = r * t;

   for (auto cit = shaders_.cbegin(); cit != shaders_.cend(); ++cit)
   {
      glUseProgram(*cit);
      proj_index_.push_back(glGetUniformLocation(*cit, shader_var[proj_index].c_str()));
      if (proj_index_.back() == -1)
         std::cerr << "WARNING: " << shader_var[proj_index] << " not found in shader program " << *cit << "." << std::endl;

      view_index_.push_back(glGetUniformLocation(*cit, shader_var[view_index].c_str()));
      if (view_index_.back() == -1)
         std::cerr << "WARNING: " << shader_var[view_index] << " not found in shader program " << *cit << "." << std::endl;

      glUniformMatrix4fv(view_index_.back(), 1, false, &view_[0][0]);
      glUniformMatrix4fv(proj_index_.back(), 1, false, &proj_[0][0]);
   }
}

void camera::set_screen(const screen_data& data)
{
   _screen_data = data;
}

void camera::move(glm::vec4 p)
{
   p *= speed_[translation];
   motion_ += p;
   updated_ = true;
}

void camera::yaw(const float y)
{
   rotation_.y = math::deg2rad(y) * speed_[rotation].y;
   auto q = versor(rotation_.y, up_);
   quat_ *= q;
   orientate();
}

void camera::pitch(const float p)
{
   rotation_.x = math::deg2rad(p) * speed_[rotation].x;
   auto q = versor(rotation_.x, right_);
   quat *= q;
   orientate();
}

void camera::roll(const float r)
{
   rotation_.z = math::deg2rad(r) * speed_[rotation].z;
   auto q = versor(rotation_.z, forward_);
   quat_ *= q;
   orientate();
}

void camera::orientate()
{
   glm::highp_mat4 r = glm::mat4_cast(quat_);
   forward_ = r * unit_forward;
   right_ = r * unit_right;
   up_ = r * unit_up;
   updated_ = true;
}

void camera::draw()
{
   if (updated_)
   {
      position_ += forward_ * -motion_.z;
      position_ += up_ * motion_.y;
      position_ += right_ * motion_.x;

      auto t = math::translate(position_);
      glm::highp_mat4 r = glm::mat4_cast(quat_);
      view_ = glm::inverse(r) * glm::inverse(t);

      for (std::size_t i = 0; i != shaders_.size(); ++i)
      {
         glUseProgram(shaders_[i]);
         glUniformMatrix4fv(view_index_[i], 1, false, &view_[0][0]);
      }

      updated_ = false;
      motion_ = { 0.0f, 0.0f, 0.0f, 0.0f };
   }
}
}
}*/
#include <doge/entity/camera.hpp>
#include <doge/utility/constants.hpp>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace doge {
namespace entity {

enum { motion, rotation };
enum { near, far };

camera::camera(const glm::vec3& position,
               const glm::vec3& rotation,
               const GLfloat aspect_ratio,
               const GLfloat field_of_view,
               const std::array<GLfloat, 2>& clipping)
   : _position{ position },
     _target{ vec3::zero },
     _direction{ glm::normalize(_position - _target) },
     _right{ glm::normalize(glm::cross(vec3::up, _target)) },
     _up{ glm::cross(_direction, _right) },
     _rotation{ rotation },
     _view{ glm::lookAt(_position, vec3::zero, vec3::up) },
     _projection{ glm::perspective(field_of_view, aspect_ratio, clipping[near], clipping[far]) },
     _field_of_view{ field_of_view },
     _aspect_ratio{ aspect_ratio },
     _clipping{ clipping },
     _updated{ false },
     view{ _view },
     projection{ _projection }
{
   update();
}

void camera::draw() //override
{
   _view = glm::lookAt(_position, _position + vec3::front, _up);
   _projection = glm::perspective(_field_of_view, _aspect_ratio, _clipping[near], _clipping[far]);
   if (_updated)
      update();
}

void camera::move(const glm::vec3& v, GLfloat time_elapsed)
{
   auto xspeed = v.x * time_elapsed;
   auto yspeed = v.y * time_elapsed;
   auto zspeed = v.z * time_elapsed;
   _position += _front * zspeed;
   _position += _right * xspeed;
   _position += _up * yspeed;
}

void camera::yaw(const GLfloat x)
{
   _rotation.x += x;
   _updated = true;
}

void camera::pitch(const GLfloat y)
{
   _rotation.y += y;
   _updated = true;
}

void camera::roll(const GLfloat z)
{
   _rotation.z += z;
   _updated = true;
}

inline void camera::update()
{
   auto xrad = glm::radians(_rotation.x);
   auto cosx = std::cos(xrad);
   auto sinx = std::sin(xrad);

   auto yrad = glm::radians(_rotation.y);
   auto cosy = std::cos(yrad);
   auto siny = std::sin(yrad);
   glm::vec3 front{ cosx * cosy, siny, sinx * cosy };
   _front = glm::normalize(front);

   _right = glm::normalize(glm::cross(_front, vec3::up));
   _up = glm::normalize(glm::cross(_right, _front));
}
}
}
