#include <doge/entity/entity.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec4.hpp>

using glm::highp_mat4;
using glm::vec4;
using glm::quat;

namespace doge {
namespace entity {
entity::entity() : transform{ _transform }, speed{ _speed }, quat{ _quat }
{
}

entity::entity(const glm::vec4& p, const glm::quat& q, const glm::vec4& s1, const glm::vec4& s2)
   : transform{ _transform }, speed{ _speed }, quat{ _quat }, _speed{{ s1, s2 }}, _quat{ q }
{
   glm::highp_mat4 r = glm::mat4_cast(q);
   glm::highp_mat4 t;
   t = glm::translate(t, -p);
   _transform = r * t;
}

void entity::set_speed(const glm::vec4& s, const doge::entity::speed i)
{
   _speed[i] = s;
}
}
}