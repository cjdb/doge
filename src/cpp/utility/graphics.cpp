#include <algorithm>
#include <array>
#include <doge/graphics/vertex_buffer.hpp>
#include <doge/utility/graphics.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <iostream>

namespace doge {
namespace graphics {
std::uint32_t index = 0;
}
namespace {
std::uint32_t index = 0;
}
void clear_colour(const std::uint32_t rgba)
{
   float r = (rgba >> 24) / 255.0f;
   float g = ((rgba & 0x00ff0000) >> 16) / 255.0f;
   float b = ((rgba & 0x0000ff00) >> 8) / 255.0f;
   float a = (rgba & 0x000000ff) / 255.0f;
   glClearColor(r, g, b, a);
}

std::uint32_t register_buffer(std::uint32_t& vbo,
                              const std::uint32_t target,
                              const std::size_t size,
                              const void* data,
                              const std::uint32_t usage,
                              std::size_t width,
                              const std::uint32_t type,
                              const std::uint32_t normalised,
                              const std::size_t stride,
                              const void* pointer)
{
   width /= sizeof(float);
   glGenBuffers(1, &vbo);
   glBindBuffer(target, vbo);
   glBufferData(target, size * sizeof(float) * width, data, usage);
   glVertexAttribPointer(index, width, type, normalised, stride, pointer);
   glEnableVertexAttribArray(index);
   return index++;
}


/*glm::quat slerp(const glm::quat& p0, const glm::quat& p1, const float t)
{
   auto d = glm::dot(p0, p1);
   if (d < 1.0f)
   {
      float omega = std::acos(d);
      float sin_omega = std::sin(omega);
      return (p0 * std::sin(1 - t) * omega / sin_omega) + (p1 * std::sin(t * omega) / sin_omega);
   }
   else
   {
      return p0;
   }
}*/

namespace ray {
glm::vec3 cast(const glm::vec2& cursor,
               const glm::highp_mat4& proj,
               const glm::highp_mat4& view,
               const int width,
               const int height)
{
   float x = (2.0f * cursor.x) / width - 1.0f;
   float y = 1.0f - (2.0f * cursor.y) / height;
   float z = 1.0f;
   glm::vec3 normalised_coordinates{ x, y, z };
   glm::vec4 clip{ x, y, -1.0f, 1.0f };
   glm::vec4 eye{ glm::inverse(proj) * clip };
   eye.z =  -1.0f;
   eye.w = 0.0f;
   glm::vec3 world{ glm::inverse(view) * eye };
   return glm::normalize(world);
}

bool intersects_plane(const glm::vec4& origin,
                      const glm::vec3& D,
                      const glm::vec3& n,
                      const float d,
                      float& intersection_distance)
{
   glm::vec3 O{ origin.x, origin.y, origin.z };
   float denominator = glm::dot(D, n);
   if (denominator == 0.0f)
      return false;
   float numerator = glm::dot(O, n) + d;

   float t = -numerator/denominator;
   if (t < 0.0f)
      return false;
   else
      intersection_distance = t;
   return true;
}

bool intersects_sphere(const glm::vec4& origin,
                       const glm::vec3& D,
                       const glm::vec3& C,
                       const float r,
                       float& intersection_distance)
{
   glm::vec3 O{ origin.x, origin.y, origin.z };
   glm::vec3 distance_to_sphere{ O - C };
   float b = glm::dot(D, distance_to_sphere);
   float c = glm::dot(distance_to_sphere, distance_to_sphere) - (r * r);
   float determinant = (b * b) - c;

   if (determinant < 0.0f)
      return false;

   float sqrt = std::sqrt(determinant);
   std::array<float, 2> t{{ -b + sqrt, -b - sqrt }};

   if (determinant == 0.0f)
   {
      if (t[0] < 0.0f)
         return false;
      else
         intersection_distance = t[0];
      return true;
   }
   else
   {
      if (t[0] < 0.0f && t[1] < 0.0f)
         return false;
      else
         intersection_distance = t[1] < 0.0f ? t[0] : t[1];
      return true;
   }
}
}
}