#include <doge/utility/constants.hpp>
#include <doge/utility/cvars.hpp>
#include <glm/vec3.hpp>

namespace doge {
namespace cvars {
const char* const screen_width{ "screen_width" };
const char* const screen_height{ "screen_height" };
const char* const antialiasing{"antialiasing" };
const char* const fullscreen{ "fullscreen" };
const char* const vsync{ "vsync" };
}

namespace hid {
bool _shift{ false };
bool _control{ false };
bool _alt{ false };
bool _super{ false };
const bool& shift{ _shift };
const bool& control{ _control };
const bool& alt{ _alt };
const bool& super{ _super };
}

namespace vec3 {
const glm::vec3 zero{ 0.0f, 0.0f, 0.0f };
const glm::vec3 up{ 0.0f, 1.0f, 0.0f };
const glm::vec3 front{ 0.0f, 0.0f, -1.0f };
}
}
