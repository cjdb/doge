#include <cstdint>
#include <deque>
#include <doge/hid/hid.hpp>
#include <doge/hid/keyboard.hpp>
#include <vector>

namespace doge {
namespace hid {
extern bool _shift, _control, _alt, _super;

namespace {
std::size_t key_count;
std::array<key_state, 360> key_mask;
std::deque<std::vector<int>> recorded_sequence;
GLFWwindow* window = nullptr;

void callback(GLFWwindow*, int key, int, int action, int mods)
{
   ++key;
   if (action == GLFW_PRESS && key_mask[key] != key_state::down)
   {
      key_mask[key] = key_state::press;
      //recorded_sequence.back().push_back(key - 1);
   }
   else if (action == GLFW_RELEASE && key_mask[key] != key_state::up)
   {
      key_mask[key] = key_state::release;
      //recorded_sequence.back().push_back(-key + 1);
   }

   ++key_count;
   _shift = mods & GLFW_MOD_SHIFT;
   _control = mods & GLFW_MOD_CONTROL;
   _alt = mods & GLFW_MOD_ALT;
   _super = mods & GLFW_MOD_SUPER;
}
}

namespace keyboard {
void init(GLFWwindow* w)
{
   window = w;
   recorded_sequence.push_back(std::vector<int>{});
   glfwSetInputMode(window, GLFW_STICKY_KEYS, true);
   glfwSetKeyCallback(window, callback);
}

void on_key_press(const int key, const std::function<void()>& f)
{
   if (key_mask[key + 1] == key_state::press)
      on_key_down(key, f);
}

void on_key_down(int key, const std::function<void()>& f)
{
   ++key;
   if (key_mask[key] >= key_state::press)
   {
      key_mask[key] = key_state::down;
      f();
   }
}

void on_key_release(const int key, const std::function<void()>& f)
{
   if (key_mask[key + 1] == key_state::release)
      on_key_up(key, f);
}

void on_key_up(int key, const std::function<void()>& f)
{
   ++key;
   if (key_mask[key] <= key_state::release)
   {
      key_mask[key] = key_state::up;
      f();
   }
}

bool key_down(int key)
{
   ++key;
   if (key_mask[key] >= key_state::press)
   {
      key_mask[key] = key_state::down;
      return true;
   }
   else
   {
      return false;
   }
}

bool key_up(int key)
{
   ++key;
   if (key_mask[key] <= key_state::press)
   {
      key_mask[key] = key_state::up;
      return true;
   }
   else
   {
      return false;
   }
}
}
}
}
