#include <doge/hid/hid.hpp>
#include <doge/hid/mouse.hpp>
#include <glm/vec2.hpp>
#include <vector>

namespace doge {
namespace hid {
extern bool _shift, _control, _alt, _super;
namespace {
enum { current, previous };
std::array<glm::vec2, 2> cursor{{ { 0.0f, 0.0f }, { 0.0f, 0.0f } }};
std::array<glm::vec2, 2> scroll{{ { 0.0f, 0.0f }, { 0.0f, 0.0f } }};
std::array<key_state, 8> key_mask{{ key_state::up, key_state::up, key_state::up, key_state::up, key_state::up, key_state::up, key_state::up, key_state::up }};
float _sensitivity = 0.5;

void button_callback(GLFWwindow*, int key, int action, int mods)
{
   if (action == GLFW_PRESS && key_mask[key] != key_state::down)
   {
      key_mask[key] = key_state::press;
      //recorded_sequence.back().push_back(key);
   }
   else if (action == GLFW_RELEASE && key_mask[key] != key_state::up)
   {
      key_mask[key] = key_state::release;
      //recorded_sequence.back().push_back(-key);
   }

   //++key_count;
   _shift = mods & GLFW_MOD_SHIFT;
   _control = mods & GLFW_MOD_CONTROL;
   _alt = mods & GLFW_MOD_ALT;
   _super = mods & GLFW_MOD_SUPER;
}

void cursor_callback(GLFWwindow*, double x, double y)
{
   cursor[previous] = cursor[current];
   cursor[current].x = static_cast<float>(x);
   cursor[current].y = static_cast<float>(y);
}

void scroll_callback(GLFWwindow*, double x, double y)
{
   scroll[previous] = scroll[current];
   scroll[current].x = static_cast<float>(x);
   scroll[current].y = static_cast<float>(y);
}
}

namespace mouse {
void init(GLFWwindow* w, const cursor_t c)
{
   glfwSetInputMode(w, GLFW_CURSOR, static_cast<int>(c));
   glfwSetInputMode(w, GLFW_STICKY_MOUSE_BUTTONS, true);
   glfwSetMouseButtonCallback(w, button_callback);
   glfwSetCursorPosCallback(w, cursor_callback);
   glfwSetScrollCallback(w, scroll_callback);
   //glfwSetCursorEnterCallback(w, enter_callback);
}

void on_key_press(const int key, const std::function<void()>& f)
{
   if (key_mask[key] == key_state::press)
      on_key_down(key, f);
}

void on_key_down(const int key, const std::function<void()>& f)
{
   if (key_mask[key] >= key_state::press)
   {
      key_mask[key] = key_state::down;
      f();
   }
}

void on_key_release(const int key, const std::function<void()>& f)
{
   if (key_mask[key] == key_state::release)
      on_key_up(key, f);
}

void on_key_up(const int key, const std::function<void()>& f)
{
   if (key_mask[key] == key_state::up)
   {
      key_mask[key] = key_state::up;
      f();
   }
}

bool key_down(const key_t key)
{
   auto k = static_cast<std::size_t>(key);
   if (key_mask[k] >= key_state::press)
   {
      key_mask[k] = key_state::down;
      return true;
   }
   else
   {
      return false;
   }
}

bool key_up(const key_t key)
{
   auto k = static_cast<std::size_t>(key);
   if (key_mask[k] < key_state::press)
   {
      key_mask[k] = key_state::up;
      return true;
   }
   else
   {
      return false;
   }
}

void on_cursor_position(const glm::vec2& v, const std::function<void()>& f)
{
   if (cursor[current] == v)
      f();
}

void on_scroll_position(const glm::vec2& v, const std::function<void()>& f)
{
   if (scroll[current] == v)
      f();
}

glm::vec2 cursor_delta()
{
   return (cursor[current] - cursor[previous]) * _sensitivity;
}

glm::vec2 scroll_delta()
{
   return scroll[current] - scroll[previous];
}

float sensitivity()
{
   return _sensitivity;
}

void sensitivity(const float s)
{
   _sensitivity = s;
}
}
}
}