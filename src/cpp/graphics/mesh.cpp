#include <iostream>
#include <cstddef>
#include <doge/graphics/mesh.hpp>

namespace doge {
namespace graphics {
mesh::mesh(const mesh_buffer& p, const element_array& e, const std::vector<texture>& t)
   : _vao{ 0 }, _position{ p }, _elements{ e }, _textures{ t }
{
   gl::GenVertexArrays(1, &_vao);
   gl::BindVertexArray(_vao);
   gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, _elements.ebo);
   gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, _elements.size(), _elements.data(), gl::STATIC_DRAW);

   gl::EnableVertexAttribArray(mesh_vertex::position);
   gl::BindVertexBuffer(mesh_vertex::position, _position.vbo, 0, _position.size());
   gl::VertexAttribFormat(mesh_vertex::position, mesh_vertex::position_size, gl::FLOAT, false, _position.buffer_width());
   gl::VertexAttribBinding(mesh_vertex::position, mesh_vertex::position);

   gl::EnableVertexAttribArray(mesh_vertex::normal);
   gl::BindVertexBuffer(mesh_vertex::normal, _position.vbo, offsetof(mesh_vertex, _normal), _position.size());
   gl::VertexAttribFormat(mesh_vertex::normal, mesh_vertex::normal_size, gl::FLOAT, false, _position.buffer_width());
   gl::VertexAttribBinding(mesh_vertex::normal, mesh_vertex::normal);

   gl::EnableVertexAttribArray(mesh_vertex::texture);
   gl::BindVertexBuffer(mesh_vertex::texture, _position.vbo, offsetof(mesh_vertex, _texture), _position.size());
   gl::VertexAttribFormat(mesh_vertex::texture, mesh_vertex::texture_size, gl::FLOAT, false, _position.buffer_width());
   gl::VertexAttribBinding(mesh_vertex::texture, mesh_vertex::texture);



   gl::BindVertexArray(0);
}

mesh::~mesh()
{
   gl::DeleteVertexArrays(1, &_vao);
}

void mesh::draw(const shader_program& s) const
{
   s.activate();
   s.set_uniform(s.get_uniform("tex_diffuse"), 1);
   for (auto cit = _textures.cbegin(); cit != _textures.cend(); ++cit)
      cit->draw();
   texture::activate(0);
   gl::BindVertexArray(_vao);
   gl::DrawElements(gl::TRIANGLES, _elements.size(), gl::UNSIGNED_INT, 0);
   gl::BindVertexArray(0);
}
}
}
