#include <doge/error/exception.hpp>
#include <doge/graphics/shader.hpp>
#include <doge/graphics/shader_program.hpp>
#include <iostream>

namespace doge {
namespace graphics {
shader_program::shader_program(const std::vector<GLuint>& s)
   : _program{ gl::CreateProgram() },
     program{ _program }
{
   for (auto shader : s)
      gl::AttachShader(_program, shader);
   gl::LinkProgram(_program);
   for (auto shader : s)
      gl::DetachShader(_program, shader);

   GLint params;
   gl::GetProgramiv(_program, gl::LINK_STATUS, &params);
   if (params == true)
   {
      std::clog << "program " << _program << " successfully linked.\n";
   }
   else
   {
      std::clog << "error << could not link shader program " << _program << '\n';
      std::clog << "======== OpenGL info log ========";
      std::clog << info_log() << '\n';
      std::clog << "======== end of info log ========" << std::endl;
      throw shader_error{ _program, info_log() };
   }
}

shader_program::~shader_program()
{
   gl::DeleteProgram(_program);
}

std::string shader_program::info_log() const
{
   GLsizei max_length = 0;
   gl::GetShaderiv(_program, gl::INFO_LOG_LENGTH, &max_length);
   std::vector<GLchar> error_log(max_length);
   gl::GetShaderInfoLog(_program, max_length, &max_length, &error_log[0]);
   return { error_log.cbegin(), error_log.cend() };
}
}
}
