#include <doge/graphics/sprite_sheet.hpp>

namespace doge {
namespace graphics {
sprite_sheet::sprite_sheet(const std::uint32_t shader,
                           vec3_vertex_buffer<GL_STATIC_DRAW>& v3b,
                           const std::vector<glm::vec3>& points,
                           const std::uint32_t columns,
                           const std::uint32_t rows,
                           const std::uint32_t index,
                           const char* sheet,
                           const char* matrix)
   : sprite(shader, v3b, points, columns, rows, index, sheet, matrix)
{
   change_sprite(index);
}

void sprite_sheet::change_sprite(const std::uint32_t index)
{
   auto column = index % columns_;
   auto row = rows_ - 1 - index / rows_;
   float s = static_cast<float>(column) / columns_;
   float t = static_cast<float>(row) / rows_;
   glUseProgram(shader_);
   glUniform2f(sprite_sheet_, s, t);
}
}
}