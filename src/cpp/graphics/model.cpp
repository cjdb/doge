#include <cstdint>
#include <doge/error/exception.hpp>
#include <doge/graphics/model.hpp>
#include <iostream>
#include <map>

namespace doge {
namespace graphics {
model::model(const std::string& path, const shader_program& shader)
{
   std::cerr << "Loading " << path << '\n';
   Assimp::Importer import;
   const auto* scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

   if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
      throw model_error{ import.GetErrorString() };
   process_node(scene->mRootNode, scene, shader, path.substr(0, path.find_last_of('/')));
   std::cerr << "Load successful\n";
}

void model::draw(const shader_program& s) const
{
   for (auto cit = _meshes.cbegin(); cit != _meshes.cend(); ++cit)
      cit->draw(s);
}

void model::process_node(const aiNode* n,
                         const aiScene* s,
                         const shader_program& shader,
                         const std::string& dir)
{
   for (GLuint i{ 0 }; i < n->mNumMeshes; ++i)
   {
      aiMesh* mesh = s->mMeshes[n->mMeshes[i]];
      _meshes.push_back(process_mesh(mesh, s, shader, dir));
   }

   for (GLuint i{ 0 }; i < n->mNumChildren; ++i)
      process_node(n->mChildren[i], s, shader, dir);
}

mesh model::process_mesh(const aiMesh* m,
                         const aiScene* s,
                         const shader_program& shader,
                         const std::string& dir)
{
   std::vector<mesh_vertex> vertices{ };
   std::vector<GLuint> indices{ };
   std::vector<texture> textures{ };

   const auto* vertex = m->mVertices;
   const auto* normal = m->mNormals;
   const auto* texture = m->mTextureCoords[0];
   for (GLuint i{ 0 }; i < m->mNumVertices; ++i, ++vertex)
   {
      // process vertex positions, normals, and texture coords
      glm::vec3 p{ vertex->x, vertex->y, vertex->z };
      glm::vec3 n{ };
      if (normal)
      {
         n = glm::vec3{ normal->x, normal->y, normal->z };
         ++normal;
      }

      glm::vec2 t{ };
      if (texture)
      {
         t = glm::vec2{ texture->x, texture->y };
         ++texture;
      }

      vertices.emplace_back(p, n, t);
   }

   // process indices
   const auto* face = m->mFaces;
   for (GLuint i{ 0 }; i < m->mNumFaces; ++i)
      for (GLuint j{ 0 }; j < face->mNumIndices; ++j)
         indices.push_back(face->mIndices[j]);

   // process material
   if (m->mMaterialIndex > 0)
   {
      const auto* material = s->mMaterials[m->mMaterialIndex];
      textures = load_material_textures(material, aiTextureType_DIFFUSE, shader, dir);
      auto specular_maps = load_material_textures(material, aiTextureType_SPECULAR, shader, dir);
      textures.insert(textures.end(), specular_maps.begin(), specular_maps.end());
   }

   return mesh{ vertices, element_array{ indices }, textures };
}

std::vector<texture> model::load_material_textures(const aiMaterial* m,
                                                   const aiTextureType type,
                                                   const shader_program& shader,
                                                   const std::string& dir)
{
   std::vector<texture> textures;
   std::map<const char*, std::size_t> texture_buffer;

   auto texture_count = m->GetTextureCount(type);
   for (GLuint i{ 0 }; i < texture_count; ++i)
   {
      aiString str;
      m->GetTexture(type, i, &str);
      auto directory = dir;
      directory.append(str.C_Str(), str.length);

      auto t = texture_buffer.find(str.C_Str());
      if (t == texture_buffer.end())
      {
         textures.emplace_back(directory, shader);
         texture_buffer.emplace(str.C_Str(), textures.size() - 1);
      }
      else
      {
         textures.emplace_back(textures[t->second]);
      }
   }

   return textures;
}
}
}
