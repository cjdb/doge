#include <doge/graphics/sprite.hpp>
#include <doge/graphics/vertex_buffer.hpp>
#include <doge/utility/graphics.hpp>
#include <doge/utility/math.hpp>
#include <utility>
#include <iostream>
namespace doge {
namespace graphics {
sprite::sprite(const shader_program& shader,
               const std::vector<GLfloat>& vertices,
               const std::vector<GLuint>& elements,
               const std::vector<buffer_data>& b,
               const std::uint32_t columns,
               const std::uint32_t rows,
               const std::uint32_t frame,
               const char* sheet,
               const char* matrix)
   : shader_{ shader },
     vertices_{ vertices },
     elements_{ elements },
     vao_{ 0 },
     vbo_{ 0 },
     ebo_{ 0 },
     columns_{ columns },
     rows_{ rows },
     frames_{ columns * rows },
     current_frame_{ frame },
     sprite_sheet_{ shader_.get_uniform(sheet) },
     sprite_matrix_{ shader_.get_uniform(matrix) }
{
   glGenVertexArrays(1, &vao_);
   glGenBuffers(1, &vbo_);
   glGenBuffers(1, &ebo_);
   glBindVertexArray(vao_);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_);
   glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices_.size(), &vertices_[0], GL_STATIC_DRAW);
   
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                sizeof(GLuint) * elements_.size(),
                &elements_[0],
                GL_STATIC_DRAW);

   for (auto it = b.cbegin(); it != b.cend(); ++it)
   {
      auto i = it - b.cbegin();
      glVertexAttribPointer(i,
                            it->width,
                            GL_FLOAT,
                            GL_FALSE,
                            it->space * sizeof(GLfloat),
                            reinterpret_cast<GLvoid*>(it->offset * sizeof(GLfloat)));
      glEnableVertexAttribArray(i);
   }
   glBindVertexArray(0);
}

sprite::sprite(const sprite& s)
   : shader_{ s.shader_ },
     vertices_{ s.vertices_ },
     elements_{ s.elements_ },
     vao_{ 0 },
     vbo_{ 0 },
     ebo_{ 0 },
     sprite_matrix_{ s.sprite_matrix_ },
     transform_{ s.transform_ }
{
   glGenVertexArrays(1, &vao_);
   glGenBuffers(1, &vbo_);
   glGenBuffers(1, &ebo_);
   glBindVertexArray(vao_);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_);
   glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices_.size(), &vertices_[0], GL_STATIC_DRAW);
   
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                sizeof(GLuint) * elements_.size(),
                &elements_[0],
                GL_STATIC_DRAW);

   glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(0));
   glEnableVertexAttribArray(0);

   glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                           reinterpret_cast<GLvoid*>(2 * sizeof(GLfloat)));
   glEnableVertexAttribArray(1);

   glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                           reinterpret_cast<GLvoid*>(6 * sizeof(GLfloat)));
   glEnableVertexAttribArray(2);

   glBindVertexArray(0);
}

sprite::sprite(sprite&& s)
   : shader_{ s.shader_ },
     vertices_{ std::move(s.vertices_) },
     elements_{ std::move(s.elements_) },
     vao_{ s.vao_ },
     vbo_{ s.vbo_ },
     ebo_{ s.ebo_ },
     transform_{ std::move(s.transform_) }
{
   s.vao_ = 0;
   s.vbo_ = 0;
   s.ebo_ = 0;
   s.transform_ = glm::highp_mat4{};
   std::cout << "moving" << std::endl;
}

sprite::~sprite()
{
   if (vbo_ != 0)
      glDeleteBuffers(1, &vbo_);

   if (ebo_ != 0)
      glDeleteBuffers(1, &ebo_);

   if (vao_ != 0)
      glDeleteVertexArrays(1, &vao_);
}

sprite& sprite::operator=(const sprite& s)
{
   sprite t{ s };
   std::swap(*this, t);
   return *this;
}

sprite& sprite::operator=(sprite&& s)
{
   if (&s != this)
   {
      vertices_ = std::move(s.vertices_);
      elements_ = std::move(s.elements_);
      vao_ = s.vao_;
      vbo_ = s.vbo_;
      ebo_ = s.ebo_;
      transform_ = std::move(s.transform_);
      s.vao_ = 0;
      s.vbo_ = 0;
      s.ebo_ = 0;
      s.transform_ = glm::highp_mat4{};
   }
   return *this;
}

void sprite::draw(const texture& tex)
{
   //glUseProgram(shader_);
   shader_.activate();
   //glEnable(GL_BLEND);
   //glDepthMask(false);
   glBindVertexArray(vao_);
   //glUniformMatrix4fv(sprite_matrix_, 1, false, &transform_[0][0]);
   shader_.set_uniform(sprite_matrix_, transform_);
   tex.draw();
   glDrawElements(GL_TRIANGLES, elements_.size(), GL_UNSIGNED_INT, nullptr);
   glBindVertexArray(0);
   //glDepthMask(true);
   //glDisable(GL_BLEND);
}

void sprite::draw(const std::vector<texture*>& t)
{
   //glEnable(GL_BLEND);
   //glDepthMask(false);
   //glUseProgram(shader_);
   shader_.activate();
   glBindVertexArray(vao_);
   //glUniformMatrix4fv(sprite_matrix_, 1, false, &transform_[0][0]);
   shader_.set_uniform(sprite_matrix_, transform_);
   for (auto it = t.begin(); it != t.end(); ++it)
      (*it)->draw();
   //e.emplace_back(index_, vbo_);
   glDrawElements(GL_TRIANGLES, elements_.size(), GL_UNSIGNED_INT, nullptr);
   glBindVertexArray(0);
   //glDepthMask(true);
   //glDisable(GL_BLEND);
}

void sprite::change_frame(std::uint32_t index)
{
   if (frames_ > 1)
   {
      index %= frames_;
      current_frame_ = index;
      auto column = current_frame_ % columns_;
      auto row = rows_ - 1 - current_frame_ / rows_;
      glm::vec2 st{ static_cast<float>(column) / columns_,
                    static_cast<float>(row) / rows_
                  };
      //glUseProgram(shader_);
      //glUniform2f(sprite_sheet_, s, t);
      shader_.activate();
      shader_.set_uniform(sprite_sheet_, st);
   }
}

void sprite::skip_frame(const std::int32_t skip)
{
   current_frame_ += skip;
   if (current_frame_ > frames_)
   {
      std::int32_t frame = static_cast<std::int32_t>(current_frame_);
      std::int32_t total_frames = static_cast<std::int32_t>(frames_);
      frame %= total_frames;
      frame += frames_;
      current_frame_ = static_cast<std::uint32_t>(frame);
   }

   change_frame(current_frame_);
}

void sprite::translate(const glm::vec4& v)
{
   transform_ *= math::translate(v);
}

void sprite::scale(const glm::vec3& v)
{
   transform_ *= math::scale(v);
}

void sprite::scale(const float f)
{
   transform_ *= math::scale(f);
}

void sprite::rotate_x(const float r)
{
   transform_ *= math::deg::rotate_x(r);
}

void sprite::rotate_y(const float r)
{
   transform_ *= math::deg::rotate_y(r);
}
void sprite::rotate_z(const float r)
{
   transform_ *= math::deg::rotate_z(r);
}
}
}