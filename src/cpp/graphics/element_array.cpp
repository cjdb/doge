#include <doge/graphics/element_array.hpp>
#include <utility>

namespace doge {
namespace graphics {

element_array::element_array(std::vector<GLuint> points) : _ebo{ 0 }, _buffer{ points }, ebo{ _ebo }
{
   gl::GenBuffers(1, &_ebo);
}

element_array::element_array(const element_array& e)
   : _ebo{ 0 },
     _buffer{ e._buffer },
     ebo{ _ebo }
{
   gl::GenBuffers(1, &_ebo);
}

element_array::element_array(element_array&& e)
   : _ebo{ e.ebo },
     _buffer{ std::move(e._buffer) },
     ebo{ _ebo }
{
   e._ebo = 0;
}

element_array::~element_array()
{
   gl::DeleteBuffers(1, &_ebo);
}

element_array& element_array::operator=(const element_array& e)
{
   element_array t{ e };
   std::swap(t, *this);
   return *this;
}

element_array& element_array::operator=(element_array&& e)
{
   if (&e != this)
   {
      gl::DeleteBuffers(1, &ebo);
      _ebo = e._ebo;
      _buffer = std::move(e._buffer);
      e._ebo = 0;
   }

   return *this;
}
}
}
