#include <array>
#include <doge/graphics/shader.hpp>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

namespace doge {
namespace graphics {
shader::shader(const std::string& path, const type t)
{
   // load shader source into memory
   std::ifstream file{ path };
   if (file.is_open() == false)
   {
      std::string message{ "ERROR: Could not load shader: " + path };
      throw std::runtime_error{ message.c_str() };
   }

   std::string text;
   while (file.eof() == false)
      text += file.get();

   file.close();

   if (text.back() == EOF)
      text.pop_back(); // eof character is read in also; need to pop that

   // create shader
   const auto* shader_code = text.c_str();
   _index = gl::CreateShader(t);
   gl::ShaderSource(_index, 1, &shader_code, nullptr);
   gl::CompileShader(_index);

   GLint compiled = 0;
   gl::GetShaderiv(_index, gl::COMPILE_STATUS, &compiled);
   if (compiled)
   {
      std::cout << "Shader " << path << " compiled as " << _index << "\n";
   }
   else
   {
      std::cerr << "Failed to compile " << path << std::endl;
      GLint length = 0;
      gl::GetShaderiv(_index, gl::INFO_LOG_LENGTH, &length);

      std::vector<GLchar> log(length);
      gl::GetShaderInfoLog(_index, length, &length, &log[0]);
      std::string error_message{ log.cbegin(), log.cend() };

      throw std::runtime_error{ "GLSL compiler error\n" + error_message + "\n----------------------------\n" + text };
   }
}

shader::~shader()
{
   gl::DeleteShader(_index);
}
}
}
