#include <algorithm>
#include <iostream>
#include <doge/graphics/texture.hpp>
#include <sstream>
#include <stb_image.h>
#include <stdexcept>

namespace doge {
namespace graphics {
texture::texture(const std::string& filename,
                 const shader_program& shader,
                 const GLuint active_texture,
                 const GLuint wrapping,
                 const GLuint min_filter,
                 const GLuint mag_filter,
                 const int channels)
   : _tex{ 0 },
     _active_texture{ active_texture },
     tex{ _tex }
{
   shader.activate();
   std::ostringstream message;

   GLint x, y, n;
   std::unique_ptr<std::uint8_t, void(&)(void*)> image(stbi_load(filename.c_str(), &x, &y, &n, channels), stbi_image_free);

   if (image == nullptr)
   {
      message << "ERROR: could not load " << filename << '.';
      throw std::runtime_error{ message.str().c_str() };
   }
   else if ((x & (x - 1)) != 0 || ((y & (y - 1)) != 0))
   {
      message << "WARNING: texture " << filename << " dimensions are not a power of two but instead"
              << " (" << x << " x " << y << ")\n";
      std::cerr << message.str();
   }

   GLuint width = sizeof(GLuint) * x;
   unsigned char* top = nullptr;
   unsigned char* bottom = nullptr;
   GLuint half_height = y / 2;

   for (GLuint row = 0; row != half_height; ++row)
   {
      top = image.get() + row * width;
      bottom = image.get() + (y - row - 1) * width;
      for (GLuint col = 0; col != width; ++col, ++top, ++bottom)
      {
         std::swap(*top, *bottom);
      }
   }

   gl::ActiveTexture(_active_texture);
   gl::GenTextures(1, &_tex);
   gl::BindTexture(gl::TEXTURE_2D, tex);
   gl::TexStorage2D(gl::TEXTURE_2D, 1, gl::RGBA8, x, y);
   gl::TexSubImage2D(gl::TEXTURE_2D, 0, 0, 0, x, y, gl::RGBA, gl::UNSIGNED_BYTE, image.get());
   switch (min_filter)
   {
   case gl::LINEAR_MIPMAP_LINEAR:
   case gl::LINEAR_MIPMAP_NEAREST:
   case gl::NEAREST_MIPMAP_LINEAR:
   case gl::NEAREST_MIPMAP_NEAREST:
      gl::GenerateMipmap(gl::TEXTURE_2D);
   default:
      break;
   }
   gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, wrapping);
   gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, wrapping);
   gl::TexParameterf(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, min_filter);
   gl::TexParameterf(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, mag_filter);
   gl::BindTexture(gl::TEXTURE_2D, 0);
}

texture::texture(const texture& t) : _tex{ t._tex }, _active_texture{ t._active_texture }, tex{ _tex }
{
}

texture::texture(texture&& t) : _tex{ t._tex }, _active_texture{ t._active_texture }, tex{ _tex }
{
   t._tex = 0;
   t._active_texture = 0;
}

texture& texture::operator=(const texture& t)
{
   if (this != &t)
   {
      texture temp{ t };
      std::swap(temp, *this);
   }

   return *this;
}

texture& texture::operator=(texture&& t)
{
   if (this != &t)
   {
      _tex = t._tex;
      _active_texture = t._active_texture;
      t._tex = 0;
      t._active_texture = 0;
   }

   return *this;
}

texture::~texture()
{
   gl::DeleteTextures(1, &_tex);
}

void texture::draw() const
{
   gl::ActiveTexture(_active_texture);
   gl::BindTexture(gl::TEXTURE_2D, tex);
}
}
}
