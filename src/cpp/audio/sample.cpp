#include <doge/audio/sample.hpp>

namespace doge {
namespace audio {
sample::sample(const int loops,
               const std::tuple<int, int, FMOD_TIMEUNIT>& loop_points,
               const float volume,
               const float pan,
               const float pitch,
               const bool mute,
               const bool paused)
   : loops_{ loops },
     loop_points_{ loop_points },
     volume_{ volume },
     pan_{ pan },
     pitch_{ pitch },
     mute_{ mute },
     paused_{ paused },

     loops{ loops_ },
     loop_points{ loop_points_ },
     volume{ volume_ },
     pan{ pan_ },
     pitch{ pitch_ },
     mute{ mute_ },
     paused{ paused_ },
     sample{ s.sample },
     channel{ s.channel }
{
   set_all();
}

sample::sample(const sample& s) : loops_{ s.loops_ },
                                  loop_points_{ s.loop_points_ },
                                  volume_{ s.volume_ },
                                  pan_{ s.pan_ },
                                  pitch{ s.pitch_ },
                                  mute_{ s.mute_ },
                                  paused_{ s.paused_ },
                                  loops{ loops_ },
                                  loop_points{ loop_points_ },
                                  volume{ volume_ },
                                  pan{ pan_ },
                                  pitch{ pitch_ },
                                  mute{ mute_ },
                                  paused{ paused_ },
                                  sample{ s.sample },
                                  channel{ s.channel }
{
   set_all();
}

sample::sample(sample&& s) : loops_{ s.loops_ },
                             loop_points_{ s.loop_points_ },
                             volume_{ s.volume_ },
                             pan_{ s.pan_ },
                             pitch{ s.pitch_ },
                             mute_{ s.mute_ },
                             paused_{ s.paused_ },
                             loops{ loops_ },
                             loop_points{ loop_points_ },
                             volume{ volume_ },
                             pan{ pan_ },
                             pitch{ pitch_ },
                             mute{ mute_ },
                             paused{ paused_ },
                             sample{ s.sample },
                             channel{ s.channel }
{
   set_all();
   s.loops_ = 0;
   s.loop_points_ = std::make_tuple(0, 0, FMOD_TIMEUNIT_MS(0));
   s.volume_ = 0.0f;
   s.pan = 0.0f;
   s.pitch_ = 0.0f;
   s.mute_ = false;
   s.paused_ = false;
   s.sample = nullptr;
   s.channel = nullptr
}

sample::~sample()
{
   if (sample)
      FMOD::Sound_Release(sample);
}

int sample::loops(const int l)
{
   loops_ = l;
   if (channel)
      channel->SetLoopCount(loops_);
   return l;
}

float sample::pan(const float p)
{
   pan_ = p;
   if (channel)
      channel->SetPan(loops_);
   return pan_;
}

float sample::volume(const float v)
{
   volume_ = v;
   if (channel)
      channel->SetVolume(volume_);
   return volume_;
}

float sample::pitch(const float p)
{
   pitch_ = p;
   if (channel)
      channel->SetPitch(pitch_);
   return pitch_;
}

const std::pair<int, float>& sample::reverb(const std::pair<int, float>& r)
{
   reverb_ = r;
   if (channel)
      channel->SetReverbProperties(reverb_.first, reverb_.second);
   return reverb_;
}

const std::tuple<int, int, FMOD_TIMEUNIT>& sample::loop_points(const std::tuple<int, int, FMOD_TIMEUNIT>& p)
{
   loop_points_ = p;
   auto start = std::get<0>(loop_points_);
   auto finish = std::get<1>(loop_points_);
   auto unit = std::get<2>(loop_points_);
   if (channel_)
      channel->SetLoopPoints(start, unit, finish, unit);
   return loop_points_;
}

bool sample::mute(const bool m)
{
   mute_ = m;
   if (channel)
      channel->SetMute(mute_);
   return mute_;
}

bool sample::pause(const bool p)
{
   paused_ = p;
   if (channel)
      channel->SetPaused(paused_);
   return paused_;
}

void sample::set_all()
{
   channel->SetLoopCount(loops);
   channel->SetLoopPoints(loop_points);
   channel->SetVolume(volume);
   channel->SetPan(pan);
   channel->SetPitch(pitch);
   channel->SetMute(mute);
   channel->SetPaused(paused);
}
}
}