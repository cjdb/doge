/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/12/23
 *
 * Provides the implementation for the exception classes.
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <sstream>
#include <string>
#include <doge/error/exception.hpp>

namespace {
inline std::string what(const GLuint i, const std::string& w)
{
   std::ostringstream os;
   os << i << ' ' << w;
   return os.str();
}
}

namespace doge {
shader_error::shader_error(const GLuint program, const std::string& what_arg) noexcept
   : std::runtime_error{ ::what(program, what_arg) }
{
}

shader_error::shader_error(const GLuint program, const char* what_arg) noexcept
   : shader_error(program, std::string{ what_arg })
{
}

model_error::model_error(const std::string& what_arg) noexcept
   : std::runtime_error{ what_arg }
{
}

model_error::model_error(const char* what_arg) noexcept
   : std::runtime_error{ what_arg }
{
}
}
