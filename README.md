Dynamically Open Game Engine (doge)
-----------------------------------
Welcome to the Dynamically Open Game Engine! This is a C++ game engine and is a demonstration of my
technical prowess using modern C++.

INSTALL
-------
To install doge, you should have the following software at your disposal:

   - gcc 5.2 OR clang 3.5
   - binutils
   - boost 1.59
   - ghc 7.10.1
   - fmod 1.06.08
   - glfw 3
   - glm
   - glew
   - stb_image

You should make sure that you have installed all of the above tools before continuing.

doge uses a minimalistic build. To build doge, simply navigate to the root folder (this folder), and
run make. This will build all the libraries necessary for you to start writing your game.

DOGE LIBRARIES
--------------
doge is a game engine. This means that it is a series of libraries that you can use to build an
actual computer game. TODO: add stuff here

COPYRIGHT AND LICENSING
-----------------------
doge is licensed under the Apache 2 license. A copy of the license is provided with every source
file.

BUGS
----
Please see BUGS.md for a list of known bugs.

FEATURE REQUESTS
----------------
Please see FEATURE_REQUESTS.md for a list of acknowledged feature requests.

AUTHORS
-------
Christopher Di Bella
