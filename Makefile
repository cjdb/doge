# Christopher Di Bella
# 2015/08/16
#
#    Copyright 2015 Christopher Di Bella
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
SHELL=/bin/bash
CPPFLAGS=-Wall -Wextra -Wc++11-compat -Werror -pedantic -I inc -pthread -std=c++14
HS=ghc

SRC=src/cpp
INC=inc
LIB=lib
FUNCTIONAL=src/hs
TESTSRC=test

all: common log thread keyboard mouse entity camera engine

common: create_folders
ifeq ($(DEBUG_LEVEL),-1)
BUILD=release
DEFINES=-DNDEBUG
else
BUILD=debug
ifeq ($(DEBUG_LEVEL),0)
else
DEBUG=-g$(DEBUG_LEVEL)
endif
endif

ifeq ($(OPTIM_LEVEL),)
ifeq ($(PLATFORM),llvm)
# Nothing happens!
else
OPTIM_LEVEL=g
endif
else
# Nothing happens!
endif

CC=g++
LINK=-lGL -lglfw -lassimp

ifeq ($(PLATFORM),)
$(error "Please specify a platform (e.g. PLATFORM=linux)")
else ifeq ($(PLATFORM),llvm)
CC=clang++
else ifeq ($(PLATFORM), win32)
#CC=x86_64-w64-mingw32-g++.exe
LINK=opengl32.dll glfw3.dll
else
# Nothing happened!
endif

BIN=bin/$(BUILD)/$(PLATFORM)
LIB=lib/$(BUILD)/$(PLATFORM)
TESTBIN=bin/test/$(BUILD)/$(PLATFORM)
CPP=$(CC) $(CPPFLAGS) -O$(OPTIM_LEVEL) $(DEBUG) $(DEFINES)

create_folders:
	mkdir -p bin bin/$(BUILD) $(BIN) lib lib/$(BUILD) $(LIB) bin/test bin/test/$(BUILD) $(TESTBIN)

experiment: camera\
            entity\
            exception\
            $(LIB)/gl_core.o\
            graphics\
            keyboard\
            mouse\
            shader\
            sprite\
            shader_program\
            shader\
            $(LIB)/stb_image.o\
            texture\
            vertex_array\
            element_array
	$(CPP) -o experiment experiment.cpp $(LIB)/camera.o\
                                       $(LIB)/entity.o\
                                       $(LIB)/exception.o\
                                       $(LIB)/element_array.o\
                                       $(LIB)/gl_core.o\
                                       $(LIB)/graphics.o\
                                       $(LIB)/keyboard.o\
                                       $(LIB)/mouse.o\
                                       $(LIB)/shader.o\
                                       $(LIB)/sprite.o\
                                       $(LIB)/stb_image.o\
                                       $(LIB)/texture.o\
                                       $(LIB)/vertex_array.o\
                                       $(LINK)

test: experiment2.cpp
	$(CPP) -o test experiment2.cpp\
                  $(LIB)/element_array.o\
                  $(LIB)/vertex_array.o\
                  $(LINK)

camera: common shader_program $(LIB)/camera.o
camera_demo: common\
             $(LIB)/camera.o\
             $(LIB)/constants.o\
             $(LIB)/element_array.o\
             $(LIB)/engine.o\
             $(LIB)/exception.o\
             $(LIB)/gl_core.o\
             $(LIB)/keyboard.o\
             $(LIB)/mesh.o\
             $(LIB)/model.o\
             $(LIB)/mouse.o\
             $(LIB)/shader.o\
             $(LIB)/shader_program.o\
             $(LIB)/stb_image.o\
             $(LIB)/texture.o\
             test/camera/camera.cpp
	$(CPP) $(LINK) -o $(TESTBIN)/camera test/camera/camera.cpp\
                                      $(LIB)/camera.o\
                                      $(LIB)/constants.o\
                                      $(LIB)/element_array.o\
                                      $(LIB)/engine.o\
                                      $(LIB)/exception.o\
                                      $(LIB)/gl_core.o\
                                      $(LIB)/keyboard.o\
                                      $(LIB)/mesh.o\
                                      $(LIB)/model.o\
                                      $(LIB)/mouse.o\
                                      $(LIB)/shader.o\
                                      $(LIB)/shader_program.o\
                                      $(LIB)/stb_image.o\
                                      $(LIB)/texture.o
clock: common $(TESTBIN)/clock
	$(TESTBIN)/clock
engine: common $(TESTBIN)/engine
	#cd $(TESTBIN)
	./engine
	#cd -
entity: common $(LIB)/entity.o
element_array: common $(LIB)/element_array.o
exception: common $(LIB)/exception.o
gl_core: common $(LIB)/gl_core.o
graphics: common $(LIB)/graphics.o
log: common $(TESTBIN)/log
keyboard: common $(LIB)/keyboard.o
memory: common $(TESTBIN)/memory
	$(TESTBIN)/memory
mesh: common $(LIB)/mesh.o
model: common $(LIB)/model.o
mouse: common $(LIB)/mouse.o
shader: common $(LIB)/shader.o
shader_program: common shader exception $(LIB)/shader_program.o
sprite: common shader_program $(LIB)/sprite.o
sprite_sheet: common $(LIB)/sprite_sheet.o
texture: common shader_program $(LIB)/texture.o
timer: common $(TESTBIN)/timer
	$(TESTBIN)/timer
testthread: common $(TESTBIN)/thread
thread: common $(LIB)/thread_pool.o
vertex_array: common $(LIB)/vertex_array.o

$(TESTBIN)/engine: $(TESTSRC)/engine.cpp\
                 $(LIB)/engine.o\
                 $(LIB)/thread_pool.o\
                 $(LIB)/keyboard.o\
                 $(LIB)/mouse.o\
                 #$(LIB)/log.o\
                 #$(LIB)/memory.o
	$(CPP) $(LINK) -o engine $(TESTSRC)/engine.cpp\
                            $(LIB)/engine.o\
                            $(LIB)/thread_pool.o\
                            $(LIB)/keyboard.o\
                            $(LIB)/mouse.o\
                            #$(LIB)/log.o $(LIB)/memory.o

$(TESTBIN)/thread: $(INC)/doge/system/thread_pool.hpp\
                   $(INC)/doge/system/thread_pool.tem\
                   $(TESTSRC)/thread_pool.cpp\
                   $(LIB)/thread_pool.o
	$(CPP) $(TESTSRC)/thread_pool.cpp $(LIB)/thread_pool.o -o $(TESTBIN)/thread
	time $(TESTBIN)/thread 1.00 0.00 0.00
	time $(TESTBIN)/thread 0.50 0.50 0.00
	time $(TESTBIN)/thread 0.50 0.25 0.25
	time $(TESTBIN)/thread 0.50 0.25 0.20
	time $(TESTBIN)/thread 0.60 0.25 0.10
	time $(TESTBIN)/thread 0.50 0.25 0.15
	time $(TESTBIN)/thread 0.40 0.30 0.10
	time $(TESTBIN)/thread 0.40 0.30 0.15
	time $(TESTBIN)/thread 0.25 0.25 0.25

$(TESTBIN)/memory: $(LIB)/memory.o\
                   $(TESTSRC)/memory.cpp
	$(CPP) $(TESTSRC)/memory.cpp $(LIB)/memory.o $(LIB)/new.o -o $(TESTBIN)/memory

$(TESTBIN)/clock: $(INC)/doge/time/clock.hpp\
                  $(INC)/doge/time/clock.inl\
                  $(TESTSRC)/time/clock.cpp
	$(CPP) $(TESTSRC)/time/clock.cpp -o $(TESTBIN)/clock

$(TESTBIN)/timer: $(INC)/doge/time/timer.hpp\
                  $(INC)/doge/time/timer.inl\
                  $(TESTSRC)/time/timer.cpp
	$(CPP) $(TESTSRC)/time/timer.cpp -o $(TESTBIN)/timer

$(LIB)/camera.o: $(INC)/doge/entity/camera.hpp\
                 $(SRC)/entity/camera.cpp
	$(CPP) -c $(SRC)/entity/camera.cpp -o $(LIB)/camera.o

$(LIB)/constants.o: $(INC)/doge/utility/constants.hpp\
                    $(INC)/doge/utility/cvars.hpp\
                    $(SRC)/utility/constants.o
	$(CPP) -c $(SRC)/utility/constants.cpp -o $(LIB)/constants.o

$(LIB)/engine.o: $(INC)/doge/engine.hpp\
                 $(SRC)/engine.cpp
	$(CPP) -c $(SRC)/engine.cpp -o $(LIB)/engine.o

$(LIB)/entity.o: $(INC)/doge/entity/entity.hpp\
                 $(SRC)/entity/entity.cpp
	$(CPP) -c $(SRC)/entity/entity.cpp -o $(LIB)/entity.o

$(LIB)/exception.o: $(INC)/doge/error/exception.hpp\
                    $(SRC)/error/exception.cpp
	$(CPP) -c $(SRC)/error/exception.cpp -o $(LIB)/exception.o

$(LIB)/gl_core.o: $(INC)/gl/gl_core_4_3.hpp\
                  $(SRC)/gl_core_4_3.cpp
	$(CPP) -c $(SRC)/gl_core_4_3.cpp -o $(LIB)/gl_core.o

$(LIB)/graphics.o: $(INC)/doge/utility/graphics.hpp\
                   $(SRC)/utility/graphics.cpp
	$(CPP) -c $(SRC)/utility/graphics.cpp -o $(LIB)/graphics.o

$(LIB)/keyboard.o: $(INC)/doge/hid/hid.hpp\
                   $(INC)/doge/hid/keyboard.hpp\
                   $(SRC)/hid/keyboard.cpp
	$(CPP) -c $(SRC)/hid/keyboard.cpp -o $(LIB)/keyboard.o

$(LIB)/memory.o: $(INC)/doge/system/memory.hpp\
                 $(SRC)/system/memory.cpp\
                 $(SRC)/system/new.cpp
	$(CPP) -c $(SRC)/system/memory.cpp -o $(LIB)/memory.o
	$(CPP) -c $(SRC)/system/new.cpp -o $(LIB)/new.o

$(LIB)/mesh.o: $(INC)/doge/graphics/vertex.hpp\
               $(INC)/doge/graphics/vertex_buffer.hpp\
               $(INC)/doge/graphics/vertex_buffer.tem\
               $(INC)/doge/graphics/element_array.hpp\
               $(INC)/doge/graphics/element_array.inl\
               $(INC)/doge/graphics/mesh.hpp\
               $(SRC)/graphics/mesh.cpp
	$(CPP) -c $(SRC)/graphics/mesh.cpp -o $(LIB)/mesh.o

$(LIB)/model.o: $(INC)/doge/graphics/mesh.hpp\
                $(INC)/doge/graphics/model.hpp\
                $(INC)/doge/graphics/shader_program.hpp\
                $(INC)/doge/graphics/texture.hpp\
                $(SRC)/graphics/model.cpp
	$(CPP) -c $(SRC)/graphics/model.cpp -o $(LIB)/model.o

$(LIB)/mouse.o: $(INC)/doge/hid/mouse.hpp\
                $(SRC)/hid/mouse.cpp
	$(CPP) -c $(SRC)/hid/mouse.cpp -o $(LIB)/mouse.o

$(LIB)/shader.o: $(INC)/doge/graphics/shader.hpp\
                 $(SRC)/graphics/shader.cpp
	$(CPP) -c $(SRC)/graphics/shader.cpp -o $(LIB)/shader.o

$(LIB)/shader_program.o: $(INC)/doge/graphics/shader_program.hpp\
                         $(INC)/doge/graphics/shader_program.inl\
                         $(SRC)/graphics/shader_program.cpp
	$(CPP) -c $(SRC)/graphics/shader_program.cpp -o $(LIB)/shader_program.o

$(LIB)/sprite.o: $(INC)/doge/graphics/sprite.hpp\
                 $(SRC)/graphics/sprite.cpp
	$(CPP) -c $(SRC)/graphics/sprite.cpp -o $(LIB)/sprite.o

$(LIB)/sprite_sheet.o: $(INC)/doge/graphics/sprite_sheet.hpp\
                       $(SRC)/graphics/sprite_sheet.cpp
	$(CPP) -c $(SRC)/graphics/sprite_sheet.cpp -o $(LIB)/sprite_sheet.o

$(LIB)/stb_image.o: $(INC)/stb_image.h\
                    src/c/stb_image.c
	gcc -I inc -Wall -o $(LIB)/stb_image.o -c src/c/stb_image.c

$(LIB)/texture.o: $(INC)/doge/graphics/texture.hpp\
                  $(SRC)/graphics/texture.cpp
	$(CPP) -c $(SRC)/graphics/texture.cpp -o $(LIB)/texture.o

$(LIB)/thread_pool.o: $(INC)/doge/system/thread_pool.hpp\
                      $(INC)/doge/system/thread_pool.tem\
                      $(SRC)/system/thread_pool.cpp
	$(CPP) -c $(SRC)/system/thread_pool.cpp -o $(LIB)/thread_pool.o

$(LIB)/vertex_array.o: $(LIB)/element_array.o\
                       $(INC)/doge/graphics/vertex_array.hpp\
                       $(SRC)/graphics/vertex_array.cpp
	$(CPP) -c $(SRC)/graphics/vertex_array.cpp -o $(LIB)/vertex_array.o

$(LIB)/element_array.o: $(INC)/doge/graphics/element_array.hpp\
                         $(INC)/doge/graphics/vertex_buffer.hpp\
                         $(INC)/doge/graphics/vertex_buffer.tem\
                         $(SRC)/graphics/element_array.cpp
	$(CPP) -c $(SRC)/graphics/element_array.cpp -o $(LIB)/element_array.o

clean:
	rm -rf $(BIN)/*.o $(LIB)/*.o $(TESTBIN)/*.o
