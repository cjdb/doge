/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/19
 *
 * Defines a thread class that joins/detaches on its own
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_SYSTEM_SMART_THREAD_HPP14_INCLUDED
#define DOGE_SYSTEM_SMART_THREAD_HPP14_INCLUDED
#include <thread>

namespace doge {
namespace system
{
enum class thread_type { joinable, detachable };

template <thread_type T>
class smart_thread
{
public:
   /**
    * Simulates the std::thread constructor
    * @param f Takes a function of type f(T1, T2, ..., Tn) that will be executed on a new thread
    * @param a All the arguments that must be supplied to f, including the <code>this</code>
    *          pointer for member functions
    */
   template <typename Callable, typename... Args>
   inline smart_thread(Callable&& f, Args&&... a);

   inline smart_thread(smart_thread<T>&&) = default;
   inline smart_thread& operator=(smart_thread<T>&&) = default;

   inline ~smart_thread();

   smart_thread(const smart_thread<T>&) = delete;
   smart_thread& operator=(const smart_thread<T>&) = delete;
private:
   std::thread thread_;
};

using joinable_thread = smart_thread<thread_type::joinable>;
using detachable_thread = smart_thread<thread_type::detachable>;
}
}

#include "smart_thread.tem"
#endif // DOGE_SYSTEM_SMART_THREAD_HPP14_INCLUDED
