#ifndef DOGE_SYSTEM_TASK_HPP_INCLUDED
#define DOGE_SYSTEM_TASK_HPP_INCLUDED
#include <functional>
#include <future>
#include <tuple>
#include <utility>

namespace doge {
namespace system {
struct base_task
{
   virtual ~base_task() = default;
   virtual void execute() = 0;
};

template <typename ReturnType, typename... Args>
struct task : base_task
{
   using type = ReturnType(Args...);
   using function_t = std::function<type>;
   using packaged_t = std::packaged_task<type>;

   packaged_t f;
   std::tuple<Args...> a;

   task(function_t&& f, Args&&... a) : f{ std::forward<function_t>(f) }, a{ std::forward<Args>(a)... }
   {
   }

   void execute() override
   {
      std::apply(f, a);
   }
};
} // namespace system
} // namespace doge
#endif // DOGE_SYSTEM_TASK_HPP_INCLUDED