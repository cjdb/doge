/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Defines the memory class used in doge
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_SYSTEM_MEMORY_LIST_HPP14_INCLUDED
#define DOGE_SYSTEM_MEMORY_LIST_HPP14_INCLUDED

#include <cstdint>
#include <limits>

namespace doge {
namespace system
{
struct node
{
   std::size_t size;
   node* next;
};

struct memory_list
{
   memory_list() : head_{ nullptr } { }

   void insert(node* n)
   {
      auto* it = head_;
      while (n < it && it->next != head_)
         it = it->next;

      n->next = it->next;
      it->next = n;

      /*while (n + n->size == n->next)
      {
         auto next = n->next;
         n->size += next->size;
         n->next = next->next;
         next->size = 0;
         next->next = nullptr;
      }*/
   }
   
   node* find(const std::size_t size)
   {
      auto* smallest = head_;

      for (auto* it = smallest; it != head_; it = it->next)
      {
         if (it->size == size)
         {
            smallest = it;
            break;
         }
         else if (it->size > size && it->size < smallest->size)
         {
            smallest = it;
         }
      }

      if (size <= smallest->size)
         return smallest;
      else
         return nullptr;
   }

   void remove(node* current, const std::size_t size)
   {
      auto* raw = reinterpret_cast<char*>(current) + size;
      auto* next = reinterpret_cast<node*>(raw);

      next->next = current->next;
      next->size = current->size - size;

      if (next->next == current)
      {
         next->next = next;
      }
      else
      {
         auto* it = next;
         while (it->next != current)
            it = it->next;
         it->next = next;
      }

      current->next = nullptr;
      current->size = size;
      if (current == head_)
         head_ = next;
   }

   node* head_;
};
}
}

#endif // DOGE_SYSTEM_MEMORY_LIST_HPP14_INCLUDED