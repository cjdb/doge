/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Defines error codes used at the lowest level
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef ERROR_CODES
#define ERROR_CODES

namespace doge {
namespace system
{
enum class error_code
{
   no_error,
   bad_allocation_size,
   insufficient_memory,
   memory_out_of_bounds
};
}
}

#endif // ERROR_CODES