/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Describes the memory class used in doge
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_MEMORY_HPP14_INCLUDED
#define DOGE_MEMORY_HPP14_INCLUDED

#include <cstdlib>
#include <mutex>

namespace doge {
namespace system
{
enum class error_code;
struct memory_list;
/**
 * Memory class defines how memory is to be allocated. It should not be instantiated by the user.
 */
class memory final
{
public:
   /**
    * @param size Defines the amount of memory to be reserved for the game engine.
    */
   memory(const std::size_t size);
   ~memory();

   /**
    * Gets the amount of available memory.
    */
   static std::size_t available() { return _available; }

   /**
    * Gets the amount of memory initially reserved.
    */
   static std::size_t total_allocation() { return _total_allocation; }

   /**
    * For internal use only. Do not call this function!
    */
   static void* malloc(std::size_t) noexcept;

   /**
    * For internal use only. Do not call this function!
    */
   static error_code free(void*) noexcept;
private:
   static std::size_t _available;
   static std::size_t _total_allocation;
};
}
}

#endif // DOGE_MEMORY_HPP14_INCLUDED