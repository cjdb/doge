/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/12/23
 *
 * Provides the interface for the thread pool class.
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_SYSTEM_THREAD_POOL_HPP14_INCLUDED
#define DOGE_SYSTEM_THREAD_POOL_HPP14_INCLUDED
#include <condition_variable>
#include <doge/structure/synchronised/queue.hpp>
#include <memory>
#include <mutex>
#include "task.hpp"
#include <queue>
#include <thread>
#include <utility>
#include <vector>

namespace doge {
namespace system {
class thread_pool final
{
   class demon;

   template <typename Callable, typename... Args>
   using future_type = typename std::result_of<Callable(Args...)>::type;

   std::size_t _thread_count;
   std::atomic_bool _finished;
   std::vector<std::thread> _pool;
   std::vector<demon> _demons;
   structure::synchronised::queue<std::shared_ptr<base_task>> _tasks;

   void task_manager();
public:
   explicit thread_pool();
   ~thread_pool();

   thread_pool(const thread_pool&) = delete;
   thread_pool& operator=(const thread_pool&) = delete;

   /**
    * Adds a function call to the list of tasks that thread_pool executes.
    * @param f The function to be executed.
    * @param a The list of arguments to be supplied to f
    * @return A std::future associated with the return type of f.
    *         Use this to get the function's return value or to sleep until the function completes
    */
   template <typename Callable, typename... Args>
   inline std::future<future_type<Callable, Args...>> add_task(Callable&& f, Args&&... a);

   /**
    * @return A shared future associated with the return type of f.
    *         Use this to get the function's return value or to sleep until the function completes
    * @see doge::system::thread_pool::add_task
    */
   template <typename Callable, typename... Args>
   inline std::shared_future<future_type<Callable, Args...>> add_shared_task(Callable&& f, Args&&... a);

   /**
    * @return The number of threads that have been instantiated.
    */
   inline std::size_t thread_count() const;

   /**
    * Dictates that it is time for the thread_pool to terminate.
    */
   inline void finish();

   /**
    * @return true if thread_pool has terminated; false otherwise
    */
   inline bool finished() const;

   /**
    * Occupies one thread and allows certain tasks to be executed on that thread only.
    * Note that this does not prevent executed tasks from deferring work to thread_pool.
    * There may be at most n / 2 demon, where n is the number of threads in thread_pool.
    *
    * A demon is effective only when there should be a background task that shouldn't be
    * interrupted, but simultaneously should not interrupt program flow (e.g. progressively
    * loading a level instead of statically loading a level). A demon shall not terminate
    * until provided with a KILL or DISOWN signal.
    *
    * @return An id to the assigned demon.
    */
   std::size_t possess_thread() noexcept;

   /**
    * Adds a function call to the list of tasks a demon shall execute.
    * @see doge::system::thread_pool::add_task
    */
   template <typename Callable, typename... Args>
   inline std::future<future_type<Callable, Args...>> demon_task(const std::size_t id, Callable&& f, Args&&... a);

   /**
    * @see doge::system::thread_pool::demon_task
    *      doge::system::thread_pool::add_shared_task
    */
   template <typename Callable, typename... Args>
   inline std::shared_future<future_type<Callable, Args...>> shared_demon_task(const std::size_t id, Callable&& f, Args&&... a);

   /**
    * Sends a KILL signal to the demon. All tasks are terminated immediately.
    * The thread returns to thread_pool and is assigned tasks as per normal.
    * @param id The id of the demon
    * @return true if the demon has been successfully killed; false otherwise
    */
   bool kill_demon(const std::size_t id) noexcept;

   /**
    * Sends a DISOWN signal to the demon, which allows an idle thread to execute tasks that
    * have been solely assinged to the demon. The demon will be terminated after all tasks
    * have been completed.
    *
    * A demon should be disowned when there is no work left for other threads to do, and the
    * demon's tasks can concurrently execute.
    *
    * @param id The id of the demon
    * @return true if the demon has been successfully disowned; false otherwise
    */
   bool disown_demon(const std::size_t id) noexcept;
private:
   class demon
   {
      std::queue<std::shared_ptr<base_task>> _tasks;
      structure::synchronised::queue<std::shared_ptr<base_task>> _disowned_tasks;
      enum class signal { available, possessing, disowned, killed, destroy };
      std::atomic<signal> _signal;
   public:
      demon();
      demon(const demon&) = delete;
      demon& operator=(const demon&) = delete;
      ~demon();

      template <typename Callable, typename... Args>
      inline std::future<future_type<Callable, Args...>> add_task(Callable&& f, Args&&... a);

      template <typename Callable, typename... Args>
      inline std::shared_future<future_type<Callable, Args...>> add_shared_task(Callable&& f, Args&&... a);

      inline bool available() const;
      inline bool disowned() const;
      inline void occupy();
      inline bool disown();
      inline bool kill();

      void possess();

      // allows a task to be executed by another thread
      void repossess();
   };
};
}
}

#include "thread_pool.tem"
#endif // DOGE_SYSTEM_THREAD_POOL_HPP14_INCLUDED
