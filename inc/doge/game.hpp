#ifndef DOGE_GAME_HPP14_INCLUDED
#define DOGE_GAME_HPP14_INCLUDED
#include <algorithm>
//#include "entity/entity.hpp"
#include <vector>
namespace doge {
class game
{
//   std::vector<shared_entity> _entities;
public:
   game() = default;
   virtual ~game() = default;

   game(const game&) = default;
   game(game&&) = default;
   game& operator=(const game&) = default;
   game& operator=(game&&) = default;

   virtual game* clone() = 0;
   virtual void frame() = 0;

   void draw() const
   {
      /*for (auto cit = _entities.cbegin(); cit != _entities.cend(); ++cit)
      {
         cit->animate();
         cit->draw();
      }*/
   }
};
}
#endif // DOGE_GAME_HPP14_INCLUDED
