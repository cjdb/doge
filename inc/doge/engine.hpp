#ifndef DOGE_ENGINE_HPP_INCLUDED
#define DOGE_ENGINE_HPP_INCLUDED
#include <gl/gl_core_4_3.hpp>
#include <doge/hid.hpp>
#include <doge/game.hpp>
#include <doge/utility/constants.hpp>
#include <GLFW/glfw3.h>
#include <memory>
#include <string>
#include <vector>

namespace doge {
struct screen_data;
class engine
{
public:
   constexpr auto use_keyboard{ true };
   constexpr auto use_mouse{ true };
   constexpr auto gl_major{ 4 };
   constexpr auto gl_minor{ 3 };
   engine(bool = use_keyboard,
          bool = use_mouse,
          int = gl_major,
          int = gl_minor);
   engine(std::vector<std::string>&& args,
          bool = use_keyboard,
          bool = use_mouse,
          int = gl_major,
          int = gl_minor);
   ~engine();

   engine(const engine&) = delete;
   engine& operator=(const engine&) = delete;

   template <typename T, typename... Args>
   void make_game(Args&&... args)
   {
      game_ = std::make_unique<T>(std::forward<Args...>(args));
   }
private:
   void create_window();
   void read_config();

   static void glfw_error(int, const char*);

   std::unique_ptr<game> game_;
};

struct screen_data
{
   int width;
   int height;
   int aliasing;
   bool fullscreen;
   bool vsync;

   std::unique_ptr<GLFWwindow, void(GLFWwindow*)> window;
   const GLFWmonitor* monitor;
   const GLFWvidmode* vidmode;

   screen_data(int width, int height, int aliasing, bool fullscreen, bool vsync);
   void make_window();
};
#endif // DOGE_ENGINE_HPP_INCLUDED