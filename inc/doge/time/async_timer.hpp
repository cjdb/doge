/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/31
 *
 * Describes the doge asynchronous timer class
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_TIME_ASYNC_TIMER_HPP14_INCLDUED
#define DOGE_TIME_ASYNC_TIMER_HPP14_INCLUDED
#include "timer.hpp"
#include "../system/thread_pool.hpp"

namespace doge {
namespace time
{
template <system::thread_type T>
class async_timer : public timer
{
public:
   template <typename Callable, typename... Args>
   async_timer(const nanoseconds duration, clock_t& reference_clock, Callable&& f, Args&&... args);

   void cancel();
private:
   system::thread_pool& pool_;
   base_task* task_;
   bool cancelled_;
   std::future<typename std::result_of<Callable(Args...)>::type> future_;

   void countdown();
};

using detachable_async_timer = async_timer<system::thread_type::detachable>;
using joinable_async_timer = async_timer<system::thread_type::joinable>;
}
}

#include "async_timer.tem"
#endif // DOGE_TIME_ASYNC_TIMER_HPP14_INCLUDED
