/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Describes the doge clock class
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_TIME_CLOCK_INL14_INCLUDED
#define DOGE_TIME_CLOCK_INL14_INCLUDED
namespace doge {
namespace time
{
clock::clock(const double speed, const bool paused, const std::chrono::high_resolution_clock::time_point& epoch) :
   epoch_{ epoch }, pause_time_{ 0_ns }, speed_{ speed }, paused_{ paused }
{
   if (paused)
      pause_time_ = epoch;
}

double clock::floating_time_since_epoch() const
{
   auto reference_time = std::chrono::high_resolution_clock::now();
   auto time = reference_time - epoch_;
   if (paused_)
      time += reference_time - pause_time_;
   return time.count() * speed_;
}

double clock::speed() const
{
   return speed_;
}

void clock::set_speed(const double s)
{
   speed_ = s;
}

bool clock::pause()
{
   if (paused_)
   {
      return false;
   }
   else
   {
      paused_ = true;
      pause_time_ = std::chrono::high_resolution_clock::now();
      return true;
   }
}

bool clock::unpause()
{
   if (paused_)
   {
      paused_ = false;
      epoch_ += (std::chrono::high_resolution_clock::now() - pause_time_);
      pause_time_ = std::chrono::high_resolution_clock::time_point{ 0_ns };
      return true;
   }
   else
   {
      return false;
   }
}

bool clock::toggle_pause()
{
   if (paused_)
      return unpause();
   else
      return pause();
}

bool clock::paused() const
{
   return paused_;
}

std::chrono::nanoseconds clock::paused_for() const
{
   return paused_ ? (std::chrono::high_resolution_clock::now() - pause_time_) : 0_ns;
}

void clock::reset(const std::chrono::high_resolution_clock::time_point& epoch)
{
   epoch_ = epoch;
}

clock& clock::operator+=(const clock& rhs)
{
   epoch_ += rhs.time_since_epoch();
   return *this;
}

clock& clock::operator-=(const clock& rhs)
{
   epoch_ -= rhs.time_since_epoch();
   return *this;
}

std::chrono::nanoseconds clock::time_since_epoch() const
{
   return std::chrono::nanoseconds{ static_cast<std::size_t>(floating_time_since_epoch()) };
}

inline bool operator==(const clock& lhs, const clock& rhs)
{
   auto reference = std::chrono::high_resolution_clock::now();
   auto t1 = (reference - (lhs.epoch_ - lhs.pause_time_)).time_since_epoch().count() / lhs.speed_;
   auto t2 = (reference - (rhs.epoch_ - rhs.pause_time_)).time_since_epoch().count() / rhs.speed_;

   return t1 == t2;
}

inline bool operator!=(const clock& lhs, const clock& rhs)
{
   return !(lhs == rhs);
}

inline clock operator+(clock lhs, const clock& rhs)
{
   lhs += rhs;
   return lhs;
}

inline clock operator-(clock lhs, const clock& rhs)
{
   lhs -= rhs;
   return lhs;
}
}
}
#endif // DOGE_TIME_CLOCK_INL14_INCLUDED
