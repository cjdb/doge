/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Describes the doge timer class
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_TIME_TIMER_HPP14_INCLDUED
#define DOGE_TIME_TIMER_HPP14_INCLUDED
#include <chrono>
#include "clock.hpp"

namespace doge {
namespace time
{
class timer
{
public:
   /**
    * Sets up the timer.
    * @param duration This is how long your timer will last in std::chrono::nanoseconds.
    * @param reference_clock The clock that the timer shall refer to. Note that the reference clock
    *                        is an external source, so you'll need to define a storage point for it
    *                        somewhere else. If a clock must explicitly be stored with the timer,
    *                        see storage_timer_t, which inherits this class and also supplies its
    *                        own reference clock.
    */
   inline timer(const std::chrono::nanoseconds& duration, clock& reference_clock);

   virtual ~timer() { }

   /**
    * Blocks program execution until the timer's duration has expired. Once called, it is
    * impossible to wake from the timer until the timer has expired.
    *
    * @pre None
    * @post None
    * @effects Blocking
    * @see doge::time::timer_t::light_sleep, doge::time::timer_t::light_sleep_in
    */
   inline void deep_sleep() const;

   /**
    * Blocks program execution until either the timer's duration has expired, or until a specified
    * portion of the timer's duration has expired. Once put to sleep, one of these conditions must
    * be satisfied prior to waking.
    *
    * @param duration The minimum amount of time to wait before waking in std::chrono::nanoseconds. If the timer
    *                 expires before this duration, the sleep will terminate prematurely.
    * @pre Timer must not have expired
    * @post None
    * @effects Blocking
    * @see doge::time::timer_t::deep_sleep, doge::time::timer_t::light_sleep_in
    */
   inline void light_sleep(const std::chrono::nanoseconds& duration) const;

   /**
    * Blocks program execution until either the timer's duration has expired, or until a specified
    * portion of the timer's duration has expired. Unlike doge::timer::timer_t::light_sleep, this
    * member function shall extend the sleep time until the specified duration has expired. This is
    * still referred to as a `light sleep', since the sleep may be terminated prior to the timer's
    * expiry.
    *
    * @param duration The minimum amount of time to wait before waking in std::chrono::nanoseconds. If the timer
    *                 expires before this duration, the sleep shall continue until the duration has
    *                 expired also.
    * @pre None
    * @post None
    * @effects Blocking, more time than timer might have passed
    * @see doge::time::timer_t::deep_sleep, doge::time_timer_t::light_sleep
    */
   inline void light_sleep_in(const std::chrono::nanoseconds& duration) const;

   /**
    * Blocks program execution until the specified duration has expired. As this function is not a
    * member function, it behaves differently to the other sleep functions. Use when you have no
    * need for an explicit timer.
    *
    * @param duration The specified amount of time to wait before continuing with program execution.
    * @param reference_clock The referfence clock associated with the sleep time.
    * @pre None
    * @post None
    * @effects Blocking
    */
   static inline void sleep_for(const std::chrono::nanoseconds& duration, clock& reference_clock);

   /**
    * Determines if the timer has reached its specified duration.
    *
    * @return true if the timer has expired; false otherwise
    * @pre Timer is not paused. If the timer is paused, this test shall return false.
    * @post None
    * @effects None
    */
   inline bool expired() const;

   /**
    * Specifies a new duration for the timer. The timer's duration is now as long as specified.
    * This does not add seconds to the timer: if 1ns is specified, the timer shall expire in
    * 1 nanosecond. If a time of zero std::chrono::nanoseconds is specified, then the timer shall be reset to
    * the last duration specified. The timer need not have expired prior to a reset.
    *
    * @param duration The new number of seconds the timer shall last for prior to expiry.
    * @pre None
    * @post Timer duration modified
    * @effects Timer duration modified
    * @see doge::time::timer_t::extend
    */
   void reset(const std::chrono::nanoseconds& duration = 0_ns);

   /**
    * Specifies an extension to the duration of a timer. This may be applied to an expired timer:
    * in this case, it has the same effect as doge::time::timer_t::reset. If 1ns is specified, and
    * the timer has not expired, then the new duration shall be
    * doge::time::timer_t::time_remaining() + 1ns.
    *
    * @param extension The number of seconds added to the timer before its expiration.
    * @pre extension > 0ns
    * @post Timer duration modified
    * @effects Timer duration modified
    * @see doge::time::timer_t::reset
    */
   void extend(const std::chrono::nanoseconds& extension);

   /**
    * @return The amount of time before the timer expires, in std::chrono::nanoseconds. The time will always be
    *         zero if the timer has expired.
    * @pre None
    * @post None
    * @effects None
    */
   inline std::chrono::nanoseconds time_remaining() const;

   /**
    * @return true if the timer is successfully paused; false otherwise
    * @pre Timer is not already paused and has not expired. Both cases will result in a return of
    *      false for this function call.
    * @post Timer is no longer active
    * @effects Modifying a mutable member
    */
   inline bool pause();

   /**
    * @return true if the timer is successfully unpaused; false otherwise
    * @pre Timer is currently paused.
    * @post Timer resumes activity.
    * @effects Modifying a mutable variable
    */
   inline bool unpause();

   /**
    * @return true if the timer is paused; false if the timer is unpaused or expired.
    * @pre Timer has not expired.
    * @post None
    * @effects None
    */
   inline bool paused() const;
protected:
   std::chrono::nanoseconds duration_;
   std::chrono::nanoseconds epoch_;
   clock& reference_clock_;
};
}
}

#include "timer.inl"
#endif // DOGE_TIME_TIMER_HPP14_INCLUDED
