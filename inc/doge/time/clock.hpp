/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/08/15
 *
 * Describes the doge clock class
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_TIME_CLOCK_HPP14_INCLUDED
#define DOGE_TIME_CLOCK_HPP14_INCLUDED
#include <chrono>
#include <doge/utility/units.hpp>

namespace doge {
namespace time
{
class clock
{
public:
   clock(const double speed = 1.0,
         const bool paused = false,
         const std::chrono::high_resolution_clock::time_point& epoch = std::chrono::high_resolution_clock::now());

   clock(const clock&) = default;
   clock& operator=(const clock&) = default;

   clock(clock&&) = default;
   clock& operator=(clock&&) = default;

   virtual ~clock() { }

   inline double floating_time_since_epoch() const;
   inline std::chrono::nanoseconds time_since_epoch() const;

   inline double speed() const;
   inline void set_speed(const double s);

   inline bool pause();
   inline bool unpause();
   inline bool toggle_pause();
   inline bool paused() const;
   inline std::chrono::nanoseconds paused_for() const;

   inline void reset(const std::chrono::high_resolution_clock::time_point& epoch = std::chrono::high_resolution_clock::now());

   inline clock& operator-=(const clock& rhs);
   inline clock& operator+=(const clock& rhs);

   friend bool operator==(const clock& lhs, const clock& rhs);
   friend bool operator!=(const clock& lhs, const clock& rhs);
private:
   std::chrono::high_resolution_clock::time_point epoch_;
   std::chrono::high_resolution_clock::time_point pause_time_;

   double speed_;
   bool paused_;
};
}
}
#include "clock.inl"
#endif // DOGE_TIME_CLOCK_HPP14_INCLUDED
