#ifndef DOGE_TIME_TIMER_INL14_INCLUDED
#define DOGE_TIME_TIMER_INL14_INCLUDED
namespace doge {
namespace time {
timer::timer(const std::chrono::nanoseconds& duration, clock& reference_clock) :
   duration_{ duration },
   epoch_{ reference_clock.time_since_epoch() },
   reference_clock_{ reference_clock }
{
}

void timer::deep_sleep() const
{
   while (expired() == false);
}

void timer::light_sleep(const std::chrono::nanoseconds& duration) const
{
   timer t{ duration, reference_clock_ };
   while (expired() == false || t.expired());
}

void timer::light_sleep_in(const std::chrono::nanoseconds& duration) const
{
   timer t{ duration, reference_clock_ };
   t.deep_sleep();
}

void timer::sleep_for(const std::chrono::nanoseconds& duration, clock& reference_clock)
{
   timer{ duration, reference_clock }.deep_sleep();
}

void timer::reset(const std::chrono::nanoseconds& duration)
{
   if (duration > 0_ns)
   {
      duration_ = duration;
   }

   epoch_ = reference_clock_.time_since_epoch();
}

bool timer::expired() const
{
   return (reference_clock_.paused() == false && time_remaining().count() < 0);
}

std::chrono::nanoseconds timer::time_remaining() const
{
   return ((duration_ + epoch_) - reference_clock_.time_since_epoch());
}

bool timer::pause()
{
   if (expired())
      return false;
   else
      return reference_clock_.pause();
}

bool timer::unpause()
{
   if (expired())
      return false;
   else
      return reference_clock_.unpause();
}

bool timer::paused() const
{
   return reference_clock_.paused();
}
}
}
#endif // DOGE_TIME_TIMER_INL14_INCLUDED
