#ifndef DOGE_AUDIO_SAMPLE_HPP14_INCLUDED
#define DOGE_AUDIO_SAMPLE_HPP14_INCLUDED
#include <fmod/fmod.hpp>
#include <mutex>
#include <string>
#include <tuple>

namespace doge {
namespace audio {
class sample
{
   int loops_;
   std::tuple<int, int, FMOD_TIMEUNIT> loop_points_;
   float volume_;
   float pan_;
   float pitch_;
   bool mute_;
   bool paused_;

   void set_all();
public:
   sample(const int loops = -1,
          const std::tuple<int, int, FMOD_TIMEUNIT>& loop_points = std::make_tuple(0, 0, FMOD_TIMEUNIT_MS),
          const float volume = 1.0f,
          const float pan = 0.0f,
          const float pitch = 1.0f,
          const bool mute = false,
          const bool paused = false);
   sample(const sample& s);
   sample(sample&& s);
   sample& operator=(const sample&);
   sample& operator=(sample&&);
   virtual ~sample();

   bool mute(const bool m);
   bool toggle_mute() { return mute(!mute); }
   bool pause(const bool p);
   bool toggle_pause() { return pause(!paused); }
   virtual bool is3d() const { return false; }

   void stop();
   bool is_playing() const;

   virtual sample* clone() = 0;

   int loops(const int l);
   float pan(const float f);
   float volume(const float v);
   float pitch(const float p);
   const std::pair<int, float>& reverb(const std::pair<int, float>& r);
   const std::tuple<int, int, FMOD_TIMEUNIT>& loop_points(const std::tuple<int, int, FMOD_TIMEUNIT>& p);

   const int& loops;
   const float& volume;
   const float& pan;
   const float& pitch;
   const bool& mute;
   const bool& paused;
   FMOD_SOUND* sample;
   FMOD_CHANNEL* channel;

   enum class type { default, no_loop, loop_normal, loop_bidirectional = 0x4, audio_2d = 0x8, audio_3d = 0x10 };
};
}
}
#endif // DOGE_AUDIO_SAMPLE_HPP14_INCLUDED