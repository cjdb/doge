#ifndef DOGE_AUDIO_AUDIO_HPP14_INCLUDED
#define DOGE_AUDIO_AUDIO_HPP14_INCLUDED
#include <fmod/fmod.hpp>
#include <cstdint>
#include <doge/audio/sample.hpp>
#include <glm/vec3.hpp>
#include <string>

namespace doge {
namespace audio {
class audio
{
   std::unordered_map<std::string, sample> samples_;
   std::unordered_map<std::size_t, sample3d> instances_;
   FMOD_SYSTEM* system_;

   audio(const audio&) = delete;
   audio& operator=(const audio&) = delete;
public:
   audio();
   ~audio();

   void update(const glm::vec3& position, const glm::vec3 forward, const glm::vec3& up);

   std::future<FMOD_RESULT> load_sample(const std::size_t& id, const std::string& path, const sample::type = sample::type::default);

   void unload_sample(const std::string& id);
   void unload_selected(const std::vector<std::string>& ids);
   void unload_unselected(const std::vector<std::string>& ids);
   void unload_all();

   void unload_sample(const std::size_t id);
   void unload_selected(const std::vector<std::size_t>& ids);
   void unload_unselected(const std::vector<std::size_t>& ids);
   void unload_all(char);

   bool play(const std::string& id);
   bool play(const std::size_t id);

   void stop(const std::string& id);
   void stop_selected(const std::vector<std::string>& ids);
   void stop_unselected(const std::vector<std::string>& ids);
   void stop_all();

   void pause(const std::string& id);
   void pause_selected(const std::vector<std::string>& ids);
   void pause_unselected(const std::vector<std::string>& ids);
   void pause_all();

   void unpause(const std::string& id);
   void unpause_selected(const std::vector<std::string>& ids);
   void unpause_unselected(const std::vector<std::string>& ids);
   void unpause_all();

   void mute(const std::string& id);
   void mute_selected(const std::vector<std::string>& ids);
   void mute_unselected(const std::vector<std::string>& ids);
   void mute_all();

   float pan(const std::string& id) const;
   void pan(const std::string& id, const float p);
   void pan_selected(const std::vector<std::string>& ids, const float p);
   void pan_unselected(const std::vector<std::string>& ids, const float p);
   void pan_all(const float p);

   float volume(const std::string& id) const;
   void volume(const std::string& id, const float v);
   void volume_selected(const std::vector<std::string>& ids, const float v);
   void volume_unselected(const std::vector<std::string>& ids, const float v);
   void volume_all(const float v);

   float pitch(const std::string& id) const;
   void pitch(const std::string& id, const float p);
   void pitch_selected(const std::vector<std::string>& ids, const float p);
   void pitch_unselected(const std::vector<std::string>& ids, const float p);
   void pitch_all(const float p);

   float loops(const std::string& id) const;
   void loops(const std::string& id, const int l);
   void loops_selected(const std::vector<std::string>& ids, const int l);
   void loops_unselected(const std::vector<std::string>& ids, const int l);
   void loops_all(const int l);

   float loop_count(const std::string& id) const;
   void loop_count(const std::string& id, const int l);
   void loop_count_selected(const std::vector<std::string>& ids, const int l);
   void loop_count_unselected(const std::vector<std::string>& ids, const int l);
   void loop_count_all(const int l);

   std::size_t make_instance(const std::string& id, const glm::vec3& p, const glm::vec3& v);
   void delete_instance(const std::size_t id);
   void delete_selected_instances(const std::vector<std::size_t>& ids);
   void delete_unselected_instances(const std::vector<std::size_t>& ids);
   void delete_all_instances();
   const glm::vec3& update_position(const std::size_t id, const glm::vec3& p);
   const glm::vec3& add_position(const std::size_t id, const glm::vec3& p);
   const glm::vec3& subtract_position(const std::size_t id, const glm::vec3& p);
   const glm::vec3& multiply_position(const std::size_t id, const glm::vec3& p);
   const glm::vec3& update_velocity(const std::size_t id, const glm::vec3& v);
   const glm::vec3& add_veclocity(const std::size_t id, const glm::vec3& v);
   const glm::vec3& subtract_velocity(const std::size_t id, const glm::vec3& v);
   const glm::vec3& multiply_velocity(const std::size_t id, const glm::vec3& v);

   bool is_playing(const std::size_t id) const;
   bool is_paused(const std::size_t id) const;
   bool is_muted(const std::size_t id) const;
};
}
}
#endif // DOGE_AUDIO_AUDIO_HPP14_INCLUDED
