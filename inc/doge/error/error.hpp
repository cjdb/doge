/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/12/23
 *
 * Provides the interface for errors.
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_ERROR_HPP14_INCLUDED
#define DOGE_ERROR_HPP14_INCLUDED
#include <cstdio>
#include <gl/gl_core_4_3.hpp>
#include <sstream>
#include <string>

namespace doge {
namespace error {
template <typename T, typename... Args>
inline std::string stream(const T& t, const Args&... a);

template <typename T>
inline std::string stream(const T& t);

template <typename T, typename... Args>
inline const char* stream(const char* str, const T& t, const Args&... a);

template <typename T>
inline const char* stream(const char* str, const T& t);
}
}
#include "error.tem"
#endif // DOGE_ERROR_HPP4_INCLUDED
