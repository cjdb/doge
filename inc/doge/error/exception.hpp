/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/12/23
 *
 * Provides the interface for the exception classes.
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef DOGE_EXCEPTION_HPP14_INCLUDED
#define DOGE_EXCEPTION_HPP14_INCLUDED
#include <gl/gl_core_4_3.hpp>
#include <stdexcept>
namespace doge {
struct shader_error : public std::runtime_error
{
   shader_error(const GLuint program, const std::string& what_arg) noexcept;
   shader_error(const GLuint program, const char* what_arg) noexcept;
};

struct model_error : public std::runtime_error
{
   model_error(const std::string& what_arg) noexcept;
   model_error(const char* what_arg) noexcept;
};
}
#endif // DOGE_EXCEPTION_HPP14_INCLUDED
