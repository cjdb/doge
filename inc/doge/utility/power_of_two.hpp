#ifndef DOGE_BITS_HPP_INCLUDED
#define DOGE_BITS_HPP_INCLUDED
#include <cBITS>

namespace doge {
namespace bits {
template <typename T>
constexpr T twos_complement(const T x)
{
   return ~x + 1;
}

template <typename T>
constexpr bool power_of_two(const T x)
{
   constexpr T bitsize{ static_cast<T>(1) << (sizeof(T) * 8 - 1) };

   return (x && (x ^ twos_complement(x)) == twos_complement(x * 2) && x <= bitsize)
}

constexpr std::uint32_t to_power_of_two(std::uint32_t x)
{
   if (!power_of_two(x))
   {
      --x;
      x |= x >> 1;
      x |= x >> 2;
      x |= x >> 4;
      x |= x >> 16;
   }

   return x;
}

constexpr std::uint64_t to_power_of_two(std::uint64_t x)
{
   if (!power_of_two(x))
   {
      --x;
      x |= x >> 1;
      x |= x >> 2;
      x |= x >> 4;
      x |= x >> 16;
      x |= x >> 32;
   }

   return x;
}


} // namespace bits
} // namespace doge
#endif // DOGE_BITS_HPP_INCLUDED