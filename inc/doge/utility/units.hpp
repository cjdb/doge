#ifndef DOGE_UNITS_HPP_INCLUDED
#define DOGE_UNITS_HPP_INCLUDED
#include <chrono>
#include <cstdint>

namespace doge {
namespace units {
constexpr std::size_t operator"" _bytes(const unsigned long long int b)
{
   return b;
}

constexpr std::size_t operator"" _KiB(const unsigned long long int b)
{
   return b * (1 << 10);
}

constexpr std::size_t operator"" _MiB(const unsigned long long int b)
{
   return b * (1 << 20);
}

constexpr std::size_t operator"" _GiB(const unsigned long long int b)
{
   return b * (1 << 30);
}

constexpr long double operator"" _nm(const long double m)
{
   return m * 1e-9;
}

constexpr long double operator"" _um(const long double m)
{
   return m * 1e-6;
}

constexpr long double operator"" _mm(const long double m)
{
   return m * 1e-3;
}

constexpr long double operator"" _m(const long double m)
{
   return m;
}

constexpr long double operator"" _km(const long double m)
{
   return m * 1e3;
}

constexpr long double operator"" _deg(const long double d)
{
   return d * 3.1415926535897932384626433832795L/180.0L;
}

constexpr long double operator"" _radians(const long double r)
{
   return r;
}

constexpr long double operator"" _degrees_farenheight(const long double f)
{
   return (f - 459.67) * 5/9;
}

constexpr long double operator"" _degrees_celcius(const long double c)
{
   return c;
}

constexpr long double operator"" _kelvin(const long double k)
{
   return k;
}

constexpr long double operator"" _cd(const long double c)
{
   return c;
}
} // namespace units
} // namespace doge
#endif // DOGE_UNITS_HPP_INCLUDED