#ifndef DOGE_CONSTANTS_HPP
#define DOGE_CONSTANTS_HPP
#include <glm/vec3.hpp>
#include <string>

namespace doge {
namespace vec3 {
extern const glm::vec3 front;
extern const glm::vec3 up;
extern const glm::vec3 zero;
} // namespace vec3

namespace cvars {
constexpr const auto screen_width{ "screen_width"s };
constexpr const auto screen_height{ "screen_height"s };
constexpr const auto antialiasing{ "antialiasing"s };
constexpr const auto vsync{ "vsync"s };
} // namespace cvars
} // namespace doge
#endif // DOGE_CONSTANTS_HPP