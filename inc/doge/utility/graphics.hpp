#ifndef DOGE_UTILITY_GRAPHICS_HPP14_INCLUDED
#define DOGE_UTILITY_GRAPHICS_HPP14_INCLUDED

#include <algorithm>
#include <cstdint>
#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <vector>

namespace doge {
void clear_colour(const std::uint32_t rgba);

std::uint32_t register_buffer(std::uint32_t& vbo,
                              const std::uint32_t target,
                              const std::size_t size,
                              const void* data,
                              const std::uint32_t usage,
                              std::size_t width,
                              const std::uint32_t type = GL_FLOAT,
                              const std::uint32_t normalised = GL_FALSE,
                              const std::size_t stride = 0,
                              const void* pointer = nullptr);

std::uint32_t register_elements(const std::uint32_t vbo,
                                const std::size_t size,
                                const void* data,
                                const std::uint32_t usage,
                                const std::uint32_t normalised = GL_FALSE,
                                const std::size_t stride = 0,
                                const void* pointer = nullptr);
template <typename T>
inline std::vector<std::uint16_t> index_vbo(const std::vector<T>& v)
{
   static std::vector<T> ordered_points;
   static std::uint32_t vbo = 0;
   static std::uint16_t index = 0;
   static std::uint16_t old_size = 0;

   std::vector<std::uint16_t> point_indicies;
   for (auto it = v.begin(); it != v.end(); ++it)
   {
      auto i = std::find(ordered_points.begin(), ordered_points.end(), *it);
      if (i == ordered_points.end())
      {
         ordered_points.emplace_back(*it);
         point_indicies.push_back(index++);
      }
      else
      {
         point_indicies.push_back(i - ordered_points.begin());
      }
   }

   if (ordered_points.size() != old_size)
   {
      if (vbo)
         glDeleteBuffers(1, &vbo);

      register_buffer(vbo, GL_ARRAY_BUFFER, ordered_points.size(), &ordered_points[0].x,
                      GL_STATIC_DRAW, sizeof(T));
      old_size = ordered_points.size();
   }

   return point_indicies;
}
namespace ray {
glm::vec3 cast(const glm::vec2& cursor,
               const glm::highp_mat4& projection, const glm::highp_mat4& view,
               const int width, const int height);
bool intersects_plane(const glm::vec3& ray_origin,
                      const glm::vec3& ray_direction,
                      const glm::vec3& plane_normal,
                      const float plane_distance);
bool intersects_sphere(const glm::vec4& ray_origin,
                       const glm::vec3& ray_direction,
                       const glm::vec3& sphere_centre,
                       const float radius,
                       float& intersection_distance);
}
}

#endif // DOGE_UTILITY_GRAPHICS_HPP14_INCLUDED