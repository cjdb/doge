/*#ifndef DOGE_CAMERA_HPP14_INCLUDED
#define DOGE_CAMERA_HPP14_INCLUDED

#include <gl/gl_core_4_3.hpp>
#include <array>
#include <cstdint>
#include <doge/entity/entity.hpp>
#include <doge/utility/units.hpp>
#include <glm/vec4.hpp>
#include <string>

namespace doge {
struct screen_data;
namespace entity {
class camera final : public entity
{
   enum { near, far };
   enum { proj_index, view_index };
   static constexpr std::size_t shader_variables = 2;
public:
   camera(const glm::vec4& position,
          const std::vector<GLuint>& shaders,
          const GLfloat aspect_ratio = 640.0f/480.0f,
          const std::array<std::string, 2> shader_var = {{ "cam_proj", "cam_view" }},
          const GLfloat field_of_view = 67.0_deg,
          const glm::vec4& forward = unit_forward,
          const glm::vec4& right = unit_right,
          const glm::vec4& up = unit_up,
          const glm::vec4& rotation = { 0.0f, 0.0f,  0.0f, 0.0f },
          const glm::vec4& translation_speed = { 5.0f, 1.0f, 5.0f, 1.0f },
          const glm::vec4& rotation_speed = { 1.0f, 1.0f, 10.0f, 1.0f },
          const std::array<GLfloat, 2>& clipping = {{ 0.1f, 100.0f }});
   ~camera() override = default;
   camera(const camera&) = default;
   camera(camera&&) = default;
   camera& operator=(const camera&) = default;
   camera& operator=(camera&&) = default;

   static void set_screen(const screen_data& s);

   void move(glm::vec4);
   void yaw(const float);
   void pitch(const float);
   void roll(const float);

   void draw() override;
   void animate() override { }
   entity* clone() override { return new camera{ *this }; }

   const glm::highp_mat4& proj;
   const glm::highp_mat4& view;
   const glm::vec4& position;
private:
   glm::highp_mat4 _proj;
   glm::highp_mat4 _view;
   glm::vec4 _forward;
   glm::vec4 _right;
   glm::vec4 _up;
   glm::vec4 _position;
   glm::vec4 _rotation;
   glm::vec4 _motion;
   std::array<GLfloat, 2> _clipping;
   GLfloat _field_of_view;
   GLfloat _aspect_ratio;
   GLfloat _range;
   std::array<GLint, shader_variables> _shader_variable;
   std::vector<GLuint> _shaders;
   std::vector<GLint> _proj_index;
   std::vector<GLint> _view_index;
   bool _updated;

   void orientate();

   static const glm::vec4 unit_forward;
   static const glm::vec4 unit_right;
   static const glm::vec4 unit_up;
   static screen_data screen_data;
};
}
}

#endif // DOGE_CAMERA_HPP14_INCLUDED*/

#ifndef DOGE_ENTITY_CAMERA_HPP14_INCLUDED
#define DOGE_ENTITY_CAMERA_HPP14_INCLUDED
#include <gl/gl_core_4_3.hpp>
#include <array>
//#include "entity.hpp"
#include <doge/utility/units.hpp>
#include <doge/utility/constants.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <string>

namespace doge {
namespace entity {
class camera final
{
   glm::vec3 _position;
   glm::vec3 _target;
   glm::vec3 _direction;
   glm::vec3 _right;
   glm::vec3 _up;
   glm::vec3 _rotation;
   glm::vec3 _front;
   glm::mat4 _view;
   glm::mat4 _projection;
   GLfloat _field_of_view;
   GLfloat _aspect_ratio;
   std::array<GLfloat, 2> _clipping;
   bool _updated;

   void update();
public:
   camera(const glm::vec3& position,
          const glm::vec3& rotation = { -90.0f, 0.0f, 0.0f },
          const GLfloat aspect_ratio = 4.0f/3.0f,
          const GLfloat field_of_view = 67.0_deg,
          const std::array<GLfloat, 2>& clipping = {{ 0.1f, 100.0f }});
   ~camera() = default;
   camera(const camera&) = default;
   camera(camera&&) = default;
   camera& operator=(const camera&) = default;
   camera& operator=(camera&&) = default;

   void draw();// override;
   void animate();// override { }
//    std::shared_ptr<entity> copy() override;
//    std::shared_ptr<entity> clone() override;

   void move(const glm::vec3& v, GLfloat time_elapsed);
   void yaw(const GLfloat);
   void pitch(const GLfloat);
   void roll(const GLfloat);

   const glm::mat4& view;
   const glm::mat4& projection;
};
}
}
#endif // DOGE_ENTITY_CAMERA_HPP14_INCLUDED
