#ifndef DOGE_ENTITY_HPP14_INCLUDED
#define DOGE_ENTITY_HPP14_INCLUDED

#include <array>
#include <cstdint>
#include <glm/fwd.hpp>
#include <memory>

namespace doge {
namespace entity {
enum speed { translation, rotation };

class entity
{
protected:
   glm::mat4 transform_;
   std::array<glm::vec4, speed_size> speed_;
   glm::quat quat_;
public:
   static constexpr std::size_t speed_size = 2;
   entity();
   entity(const glm::vec4& p,
          const glm::quat& q,
          const glm::vec4& st = { 1.0f, 1.0f, 1.0f, 1.0f },
          const glm::vec4& sr = { 10.0f, 10.0f, 10.0f, 1.0f });
   virtual ~entity() = default;
   entity(const entity&) = default;
   entity(entity&&) = default;
   entity& operator=(const entity&) = default;
   entity& operator=(entity&&) = default;

   virtual std::shared_ptr<entity> copy() const = 0;
   virtual std::shared_ptr<entity> clone() const = 0;
   virtual void draw() = 0;
   virtual void animate() = 0;

   void set_speed(const glm::vec4& s, const speed i = translation);

   decltype(transform_)& transform;
   decltype(speed_)& speed;
   decltype(quat_)& quat;
};
}
}
#endif // DOGE_ENTITY_HPP14_INCLUDED
