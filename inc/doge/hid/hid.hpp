#ifndef DOGE_HID_HID_HPP14_INCLUDED
#define DOGE_HID_HID_HPP14_INCLUDED
namespace doge {
namespace hid {
extern const bool& shift;
extern const bool& control;
extern const bool& alt;
extern const bool& super;

enum class key_state { up, release, press, down };
} // namespace hid
} // namespace doge
#endif // DOGE_HID_HID_HPP14_INCLUDED