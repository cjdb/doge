#ifndef DOGE_MOUSE_HPP_INCLUDED
#define DOGE_MOUSE_HPP_INCLUDED
#include <functional>
#include <GLFW/glfw3.h>
#include <glm/fwd.hpp>
namespace doge {
namespace hid {
namespace mouse {
enum class cursor_t { normal = GLFW_CURSOR_NORMAL, hidden = GLFW_CURSOR_HIDDEN, disabled = GLFW_CURSOR_DISABLED };
enum class key_t { lmb, rmb, mmb, key3, key4, key5, key6, key7 };

void poll();

void init(GLFWwindow*, const cursor_t = cursor_t::normal);

void on_key_press(const int key, const std::function<void()>& f);
void on_key_down(const int key, const std::function<void()>& f);
void on_key_release(const int key, const std::function<void()>& f);
void on_key_up(const int key, const std::function<void()>& f);

bool key_down(const key_t key);
bool key_up(const key_t key);

void on_cursor_position(const glm::vec2& v, const std::function<void()>& f);
void on_scroll_position(const glm::vec2& v, const std::function<void()>& f);

bool cursor_at_position(const glm::vec2& pos);
bool cursor_in_range(const glm::vec2& min, const glm::vec2& max);
bool scroll_at_position(const glm::vec2& pos);
bool scroll_in_range(const glm::vec2& min, const glm::vec2& max);
glm::vec2 cursor_delta();
glm::vec2 scroll_delta();

void enable();
void disable();
} // namespace mouse
} // namespace hid
} // namespace doge
#endif // DOGE_MOUSE_HPP_INCLUDED
