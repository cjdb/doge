#ifndef DOGE_KEYBOARD_HPP14_INCLUDED
#define DOGE_KEYBOARD_HPP14_INCLUDED
#include <functional>
#include <GLFW/glfw3.h>

namespace doge {
namespace hid {
namespace keyboard {
void poll();

void init(GLFWwindow*);

void on_key_press(const int key, const std::function<void()>& f);
void on_key_down(const int key, const std::function<void()>& f);
void on_key_release(const int key, const std::function<void()>& f);
void on_key_up(const int key, const std::function<void()>& f);

bool key_down(const int key);
bool key_up(const int key);
} // namespace keyboard
} // namespace hid
} // namespace doge
#endif // DOGE_KEYBOARD_HPP14_INCLUDED
