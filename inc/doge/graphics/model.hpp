#ifndef DOGE_GRAPHICS_MODEL_HPP14_INCLUDED
#define DOGE_GRAPHICS_MODEL_HPP14_INCLUDED
#include <gl/gl_core_4_3.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "mesh.hpp"
#include "shader_program.hpp"
#include <string>
#include "texture.hpp"
#include <vector>

namespace doge {
namespace graphics {
class model
{
   std::vector<mesh> _meshes;

   void process_node(const aiNode*, const aiScene*, const shader_program&, const std::string&);
   mesh process_mesh(const aiMesh*, const aiScene* s, const shader_program&, const std::string&);
   std::vector<texture> load_material_textures(const aiMaterial*,
                                               const aiTextureType,
                                               const shader_program&,
                                               const std::string&);
public:
   model(const std::string& path, const shader_program& shader);
   model(const char* path, const shader_program& shader) : model(std::string{ path }, shader) { }

   void draw(const shader_program& s) const;
};
}
}
#endif // DOGE_GRAPHICS_MODEL_HPP14_INCLUDED
