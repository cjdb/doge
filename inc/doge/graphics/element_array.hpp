#ifndef DOGE_GRAPHICS_ELEMENT_BUFFER_HPP14_INCLUDED
#define DOGE_GRAPHICS_ELEMENT_BUFFER_HPP14_INCLUDED
#include <gl/gl_core_4_3.hpp>
#include "vertex_buffer.hpp"
#include <vector>
namespace doge {
namespace graphics {
class element_array
{
   GLuint _ebo;
   std::vector<GLuint> _buffer;
public:
   template <typename T>
   inline element_array(vertex_buffer<T>& vbo, const std::vector<T>& points);
   element_array(std::vector<GLuint>);
   element_array(const element_array&);
   element_array(element_array&&);
   ~element_array();

   element_array& operator=(const element_array&);
   element_array& operator=(element_array&&);

   inline GLuint size() const;
   inline const GLuint* data() const;

   const GLuint& ebo;
};
}
}
#include "element_array.inl"
#endif // DOGE_GRAPHICS_ELEMENT_BUFFER_HPP14_INCLUDED
