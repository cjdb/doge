#ifndef DOGE_GRAPHICS_ELEMENT_BUFFER_INL14_INCLUDED
#define DOGE_GRAPHICS_ELEMENT_BUFFER_INL14_INCLUDED

namespace doge {
namespace graphics {
template <typename T>
element_array::element_array(vertex_buffer<T>& vbo, const std::vector<T>& points)
   : _ebo{ 0 },
     _buffer{ vbo.insert(points) },
     ebo{ _ebo }
{
   gl::GenBuffers(1, &_ebo);
}

GLuint element_array::size() const
{
   return sizeof(GLuint) * _buffer.size();
}

const GLuint* element_array::data() const
{
   return _buffer.data();
}
}
}
#endif // DOGE_GRAPHICS_ELEMENT_BUFFER_INL14_INCLUDED
