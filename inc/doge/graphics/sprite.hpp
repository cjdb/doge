#ifndef DOGE_GRAPHICS_SPRITE_HPP14_INCLUDED
#define DOGE_GRAPHICS_SPRITE_HPP14_INCLUDED

#include <cstdint>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include "shader_program.hpp"
#include "texture.hpp"
#include <utility>
#include <vector>
#include "vertex_array.hpp"

namespace doge {
namespace graphics {
class sprite
{
public:
   sprite(const shader_program& shader,
          const std::vector<GLfloat>& vertices,
          const std::vector<GLuint>& elements,
          const std::vector<buffer_data>& b,
          const std::uint32_t columns = 1,
          const std::uint32_t rows = 1,
          const std::uint32_t frame = 1,
          const char* shader_sheet = "sheet",
          const char* shader_matrix = "matrix");
   sprite(const sprite& s);
   sprite(sprite&& s);
   ~sprite();

   sprite& operator=(const sprite& s);
   sprite& operator=(sprite&& s);

   void draw(const texture& t);
   void draw(const std::vector<texture*>&);
   void translate(const glm::vec4& v);
   void scale(const glm::vec3& v);
   void scale(const float f);
   void rotate_x(const float);
   void rotate_y(const float);
   void rotate_z(const float);

   void change_frame(std::uint32_t index);
   void skip_frame(const std::int32_t skip = 0);
   void next_frame() { ++current_frame_; skip_frame(); }
   void prev_frame() { --current_frame_; skip_frame(); }
protected:
   const shader_program& shader_;
   std::vector<GLfloat> vertices_;
   std::vector<GLuint> elements_;
   GLuint vao_;
   GLuint vbo_;
   GLuint ebo_;
   std::uint32_t index_;
   std::uint32_t columns_;
   std::uint32_t rows_;
   std::uint32_t frames_;
   std::uint32_t current_frame_;
   std::int32_t sprite_sheet_;
   std::int32_t sprite_matrix_;
   glm::highp_mat4 transform_;
};
}
}
#endif // DOGE_GRAPHICS_SPRITE_HPP14_INCLUDED