#ifndef DOGE_GRAPHICS_VERTEX_HPP14_INCLUDED
#define DOGE_GRAPHICS_VERTEX_HPP14_INCLUDED
#include <cstdint>
#include <gl/gl_core_4_3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace doge {
namespace graphics {
struct sprite_vertex
{
   glm::vec2 position_;
   glm::vec4 colour_;
   glm::vec2 texture_;

   enum type_index { position, colour, texture };
   static constexpr GLuint position_size{ sizeof(position_) / sizeof(GLfloat) };
   static constexpr GLuint colour_size{ sizeof(colour_) / sizeof(GLfloat) };
   static constexpr GLuint texture_size{ sizeof(texture_) / sizeof(GLfloat) };
   static constexpr std::size_t index{ 3 };
};

struct mesh_vertex
{
   glm::vec3 position_;
   glm::vec3 normal_;
   glm::vec2 texture_;

   mesh_vertex(const glm::vec3& p, const glm::vec3& n, const glm::vec2& t)
      : position_{ p }, normal_{ n }, texture_{ t }
   {
   }

   enum type_index { position, normal, texture };
   static constexpr GLuint position_size{ sizeof(position_) / sizeof(GLfloat) };
   static constexpr GLuint normal_size{ sizeof(normal_) / sizeof(GLfloat) };
   static constexpr GLuint texture_size{ sizeof(texture_) / sizeof(GLfloat) };
   static constexpr std::size_t index{ 3 };
};
}
}
#endif // DOGE_GRAPHICS_VERTEX_HPP14_INCLUDED
