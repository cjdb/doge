#ifndef DOGE_GRAPHICS_VERTEX_BUFFER_HPP14_INCLUDED
#define DOGE_GRAPHICS_VERTEX_BUFFER_HPP14_INCLUDED
#include <algorithm>
#include <array>
#include <gl/gl_core_4_3.hpp>
#include <utility>
#include <vector>
#include "vertex.hpp"

namespace doge {
namespace graphics {
template <typename T>
class vertex_buffer
{
   using type_index = typename T::type_index;
   GLuint _vbo;
   std::vector<T> _buffer;
public:
   inline vertex_buffer(const std::vector<T>& points = {});
   inline vertex_buffer(const vertex_buffer<T>&);
   inline vertex_buffer(vertex_buffer<T>&&);
   inline ~vertex_buffer();

   inline vertex_buffer<T>& operator=(const vertex_buffer<T>&);
   inline vertex_buffer<T>& operator=(vertex_buffer<T>&&);

   inline std::vector<GLuint> insert(const std::vector<T>&);

   inline std::size_t size() const;
   inline std::size_t buffer_width() const;
   inline const T* data() const;

   const GLuint& vbo;
};

using sprite_buffer = vertex_buffer<sprite_vertex>;
using mesh_buffer = vertex_buffer<mesh_vertex>;
}
}
#include "vertex_buffer.tem"
#endif // DOGE_GRAPHICS_VERTEX_BUFFER_HPP14_INCLUDED
