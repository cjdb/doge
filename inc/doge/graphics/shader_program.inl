#ifndef DOGE_GRAPHICS_SHADER_PROGRAM_INL14_INCLUDED
#define DOGE_GRAPHICS_SHADER_PROGRAM_INL14_INCLUDED
namespace doge {
namespace graphics {
void shader_program::set_uniform(const GLint location, const GLfloat v) const
{
   gl::Uniform1f(location, v);
}

void shader_program::set_uniform(const GLint location, const glm::vec2& v) const
{
   gl::Uniform2f(location, v.x, v.y);
}

void shader_program::set_uniform(const GLint location, const glm::vec3& v) const
{
   gl::Uniform3f(location, v.x, v.y, v.z);
}

void shader_program::set_uniform(const GLint location, const glm::vec4& v) const
{
   gl::Uniform4f(location, v.x, v.y, v.z, v.w);
}

void shader_program::set_uniform(const GLint location, const GLint v) const
{
   gl::Uniform1i(location, v);
}

void shader_program::set_uniform(const GLint location, const std::array<GLint, 2>& v) const
{
   gl::Uniform2i(location, v[0], v[1]);
}

void shader_program::set_uniform(const GLint location, const std::array<GLint, 3>& v) const
{
   gl::Uniform3i(location, v[0], v[1], v[2]);
}

void shader_program::set_uniform(const GLint location, const std::array<GLint, 4>& v) const
{
   gl::Uniform4i(location, v[0], v[1], v[2], v[3]);
}

void shader_program::set_uniform(const GLint location, const GLuint v) const
{
   gl::Uniform1ui(location, v);
}

void shader_program::set_uniform(const GLint location, const std::array<GLuint, 2>& v) const
{
   gl::Uniform2ui(location, v[0], v[1]);
}

void shader_program::set_uniform(const GLint location, const std::array<GLuint, 3>& v) const
{
   gl::Uniform3ui(location, v[0], v[1], v[2]);
}

void shader_program::set_uniform(const GLint location, const std::array<GLuint, 4>& v) const
{
   gl::Uniform4ui(location, v[0], v[1], v[2], v[3]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<GLfloat, Size>& v) const
{
   gl::Uniform1fv(location, Size, &v[0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<glm::vec2, Size>& v) const
{
   gl::Uniform2fv(location, Size, &v[0].x);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<glm::vec3, Size>& v) const
{
   gl::Uniform3fv(location, Size, &v[0].x);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<glm::vec4, Size>& v) const
{
   gl::Uniform4fv(location, Size, &v[0].x);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<GLint, Size>& v) const
{
   gl::Uniform1iv(location, Size, &v[0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<std::array<GLint, 2>, Size>& v) const
{
   gl::Uniform2iv(location, Size, &v[0][0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<std::array<GLint, 3>, Size>& v) const
{
   gl::Uniform3iv(location, Size, &v[0][0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<std::array<GLint, 4>, Size>& v) const
{
   gl::Uniform4iv(location, Size, &v[0][0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<GLuint, Size>& v) const
{
   gl::Uniform1uiv(location, Size, &v[0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<std::array<GLuint, 2>, Size>& v) const
{
   gl::Uniform2uiv(location, Size, &v[0][0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<std::array<GLuint, 3>, Size>& v) const
{
   gl::Uniform3uiv(location, Size, &v[0][0]);
}

template <GLsizei Size>
void shader_program::set_uniform(const GLint location, const std::array<std::array<GLuint, 4>, Size>& v) const
{
   gl::Uniform4uiv(location, Size, &v[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat2& m, const GLboolean transpose) const
{
   gl::UniformMatrix2fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat3& m, const GLboolean transpose) const
{
   gl::UniformMatrix3fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat4& m, const GLboolean transpose) const
{
   gl::UniformMatrix4fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat2x3& m, const GLboolean transpose) const
{
   gl::UniformMatrix2x3fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat2x4& m, const GLboolean transpose) const
{
   gl::UniformMatrix2x4fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat3x2& m, const GLboolean transpose) const
{
   gl::UniformMatrix3x2fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat3x4& m, const GLboolean transpose) const
{
   gl::UniformMatrix3x4fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat4x2& m, const GLboolean transpose) const
{
   gl::UniformMatrix4x2fv(location, 1, transpose, &m[0][0]);
}

void shader_program::set_uniform(const GLint location, const glm::highp_mat4x3& m, const GLboolean transpose) const
{
   gl::UniformMatrix4x3fv(location, 1, transpose, &m[0][0]);
}

GLint shader_program::get_uniform(const GLchar* name) const
{
   return gl::GetUniformLocation(_program, name);
}

void shader_program::activate() const
{
   gl::UseProgram(_program);
}
}
}

#endif // DOGE_GRAPHICS_SHADER_PROGRAM_INL14_INCLUDED
