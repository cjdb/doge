#ifndef DOGE_GRAPHICS_MESH_HPP14_INCLUDED
#define DOGE_GRAPHICS_MESH_HPP14_INCLUDED
#include <gl/gl_core_4_3.hpp>
#include "element_array.hpp"
#include "texture.hpp"
#include "vertex.hpp"
#include "vertex_buffer.hpp"
#include <vector>

namespace doge {
namespace graphics {
class mesh
{
   GLuint _vao;
   mesh_buffer _position;
   element_array _elements;
   std::vector<texture> _textures;
public:
   mesh(const mesh_buffer& p, const element_array& e, const std::vector<texture>& t);
   mesh(const mesh&) = delete;
   mesh(mesh&&) = default;
   mesh& operator=(const mesh&) = delete;
   mesh& operator=(mesh&&) = default;
   ~mesh();
   void draw(const shader_program& s) const;
};
}
}
#endif // DOGE_GRAPHICS_MESH_HPP14_INCLUDED
