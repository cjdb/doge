#ifndef DOGE_GRAPHICS_TEXTURE_HPP14_INCLUDED
#define DOGE_GRAPHICS_TEXTURE_HPP14_INCLUDED

#include <array>
#include <gl/gl_core_4_3.hpp>
#include <glm/fwd.hpp>
#include "shader_program.hpp"
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace doge {
namespace graphics {
class texture
{
   GLuint _tex;
   GLuint _active_texture;
public:
   texture(const std::string&    filename,
           const shader_program& shader,
           const GLuint          active_texture = gl::TEXTURE0,
           const GLuint          wrapping       = gl::CLAMP_TO_EDGE,
           const GLuint          min_filter     = gl::LINEAR_MIPMAP_LINEAR,
           const GLuint          mag_filter     = gl::LINEAR,
           const int             channels       = 4);
   texture(const texture& t);
   texture(texture&& t);
   texture& operator=(const texture& rhs);
   texture& operator=(texture&& rhs);
   ~texture();

   void draw() const;
   static void activate(const GLuint t) { gl::ActiveTexture(gl::TEXTURE0 + t); }

   const std::uint32_t& tex;
};
}
}
#endif // DOGE_TEXTURE_HPP14_INCLUDED
