#ifndef DOGE_GRAPHICS_SPRITE_SHEET_HPP14_INCLUDED
#define DOGE_GRAPHICS_SPRITE_SHEET_HPP14_INCLUDED
#include "sprite.hpp"

namespace doge {
namespace graphics {
class sprite_sheet : public sprite
{
public:
   sprite_sheet(const std::uint32_t shader,
                vec3_vertex_buffer<GL_STATIC_DRAW>& v3b,
                const std::vector<glm::vec3>& points,
                const std::uint32_t columns,
                const std::uint32_t rows,
                const std::uint32_t index = 0,
                const char* sheet_sampler = "sprite_sheet",
                const char* shader_matrix = "sprite_matrix");

   sprite_sheet(const sprite_sheet&);
   sprite_sheet(sprite_sheet&&);
   sprite_sheet& operator=(const sprite_sheet&);
   sprite_sheet& operator=(sprite_sheet&&);
   void change_sprite(const std::uint32_t index);
};
}
}
#endif // DOGE_GRAPHICS_SPRITE_SHEET_HPP14_INCLUDED