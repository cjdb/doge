#ifndef DOGE_GRAPHICS_SHADER_PROGRAM_HPP14_INCLUDED
#define DOGE_GRAPHICS_SHADER_PROGRAM_HPP14_INCLUDED
#include <array>
#include <gl/gl_core_4_3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat2x2.hpp>
#include <glm/mat2x3.hpp>
#include <glm/mat2x4.hpp>
#include <glm/mat3x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat3x4.hpp>
#include <glm/mat4x2.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat4x4.hpp>
#include <string>
#include <vector>

namespace doge {
namespace graphics {
class shader_program
{
   GLuint _program;
public:
   shader_program(const std::vector<GLuint>& s);
   ~shader_program();

   inline void set_uniform(const GLint location, const GLfloat v) const;
   inline void set_uniform(const GLint location, const glm::vec2& v) const;
   inline void set_uniform(const GLint locaiton, const glm::vec3& v) const;
   inline void set_uniform(const GLint location, const glm::vec4& v) const;

   inline void set_uniform(const GLint location, const GLint v) const;
   inline void set_uniform(const GLint location, const std::array<GLint, 2>& v) const;
   inline void set_uniform(const GLint location, const std::array<GLint, 3>& v) const;
   inline void set_uniform(const GLint location, const std::array<GLint, 4>& v) const;

   inline void set_uniform(const GLint location, const GLuint v) const;
   inline void set_uniform(const GLint location, const std::array<GLuint, 2>& v) const;
   inline void set_uniform(const GLint location, const std::array<GLuint, 3>& v) const;
   inline void set_uniform(const GLint location, const std::array<GLuint, 4>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<GLfloat, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<glm::vec2, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<glm::vec3, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<glm::vec4, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<GLint, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<std::array<GLint, 2>, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<std::array<GLint, 3>, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<std::array<GLint, 4>, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<GLuint, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<std::array<GLuint, 2>, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<std::array<GLuint, 3>, Size>& v) const;

   template <GLsizei Size>
   inline void set_uniform(const GLint location, const std::array<std::array<GLuint, 4>, Size>& v) const;

   inline void set_uniform(const GLint location, const glm::highp_mat2& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat3& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat4& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat2x3& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat2x4& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat3x2& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat3x4& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat4x2& m, const GLboolean transpose = false) const;
   inline void set_uniform(const GLint location, const glm::highp_mat4x3& m, const GLboolean transpose = false) const;

   inline GLint get_uniform(const GLchar* name) const;

   inline void activate() const;
   std::string info_log() const;

   const GLuint& program;
};
}
}
#include "shader_program.inl"
#endif // DOGE_GRAPHICS_SHADER_HPP14_INCLUDED
