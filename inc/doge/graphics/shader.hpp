#ifndef DOGE_GRAPHICS_SHADER_HPP14_INCLUDED
#define DOGE_GRAPHICS_SHADER_HPP14_INCLUDED
#include <cstdint>
#include <gl/gl_core_4_3.hpp>
#include <string>

namespace doge {
namespace graphics {

class shader
{
public:
   enum type { compute = gl::COMPUTE_SHADER, fragment = gl::FRAGMENT_SHADER, geometry = gl::GEOMETRY_SHADER, tesselation_control = gl::TESS_CONTROL_SHADER, tesselation_evaluation = gl::TESS_EVALUATION_SHADER, vertex = gl::VERTEX_SHADER };
   shader(const std::string& path, const type t);
   ~shader();

   shader(const shader&) = delete;
   shader(shader&&) = default;

   shader& operator=(const shader&) = delete;
   shader& operator=(shader&&) = default;

   operator GLuint() { return _index; }
private:
   GLuint _index;
};
}
}

#endif // DOGE_GRAPHICS_SHADER_HPP14_INCLUDED
