#ifndef DOGE_STRUCTURE_SYNCHRONISED_QUEUE_HPP14_INCLUDED
#define DOGE_STRUCTURE_SYNCHRONISED_QUEUE_HPP14_INCLUDED
#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>
#include <utility>

namespace doge {
namespace structure {
namespace synchronised {
template <typename T>
class queue
{
   std::mutex _mutex;
   std::queue<T> _queue;
   std::condition_variable _condition;
public:
   inline queue(std::queue<T>&& q = std::queue<T>());
   inline queue(const std::queue<T>& q);
   ~queue() = default;

   inline void push(T value);

   inline void wait_and_pop(T& value);
   inline std::shared_ptr<T> wait_and_pop();
   inline bool try_pop(T& value);
   inline std::shared_ptr<T> try_pop();

   inline bool empty() const;
   inline void clear();

   inline queue<T>& move(std::queue<T>&& q);
};
}
}
}
#include "queue.tem"
#endif // DOGE_STRUCTURE_SYNCHRONISED_QUEUE_HPP14_INCLUDED
