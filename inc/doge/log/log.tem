#ifndef DOGE_LOG_TEM_INCLUDED
#define DOGE_LOG_TEM_INCLUDED
namespace doge {
template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>::basic_log(const bool debug)
   : buffer_size_{ debug ? 32, 2048 }
{
   constexpr auto epoch{ 1900 };
   auto current_time{ std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) };
   auto now{ std::localtime(&current_time) };

   auto oss = std::ostringstream{ };
   oss << "logs/doge-"
       << std::setfill('0')
       << std::setw(2) << now->tm_hour
       << std::setw(2) << now->tm_min
       << std::setw(2) << now->tm_sec << '-'
       << std::setw(2) << now->tm_year + epoch << '-'
       << std::setw(2) << now->tm_mon  + 1 << '-'
       << std::setw(2) << now->tm_mday + 1;
   filename_ = oss.str();
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>::~basic_log()
{
   unlocked_put(event);
   unlocked_put("doge::basic_log end\n");
   unlocked_flush();
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::operator<<(std::basic_ostream<CharT, Traits>& (*pf)(std::basic_ostream<CharT, Traits>&))
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(pf);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_put(std::basic_ostream<CharT, Traits>& (*pf)(std::basic_ostream<CharT, Traits>&))
{
   if (pf == &std::endl<CharT, Traits>)
   {
      message_.put('\n');
      unlocked_flush();
   }
   else if (pf == &std::flush<CharT, Traits>)
   {
      unlocked_flush();
   }
   else
   {
      pf(message_);
   }
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::operator<<(std::ios& (*pf)(std::ios&))
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(pf);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_put(std::ios& (*pf)(std::ios&))
{
   pf(message_);
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::operator<<(std::ios_base& (*pf)(std::ios_base&))
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(pf);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_put(std::ios_base& (*pf)(std::ios_base&))
{
   pf(message_);
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::operator<<(const std::function<log_t(log_t&)>& f)
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(f);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_put(const std::function<void(log_t&)>& f)
{
   unlocked_flush();
   message_type_ = f;
   message_type_(*this);
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::operator<<(std::function<log_t(log_t&)>&& f)
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(f);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_put(std::function<void(log_t&)>&& f)
{
   unlocked_flush();
   message_type_ = std::move(f);
   message_type_(*this);
}

template <typename CharT, typename Traits, typename Allocator>
template <typename T>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::operator<<(const T& o)
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(o);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
template <typename T>
void basic_log<CharT, Traits, Allocator>::unlocked_put(const T& o)
{
   message_ << o;
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::flush()
{
   std::lock_guard<std::mutex> lock{ lock_ };
   return unlocked_flush();
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::unlocked_flush()
{
   const auto& message{ message_.str() };
   if (!message.empty())
   {
      if (message.back() != '\n';
         message_ << '\n';
      for (auto i{ 0 }; i < 32; ++i)
         message_ << "----";
      message_ << '\n';

      buffered_messages_.push_back(message_.str());

      if (buffered_messages_.size() == buffer_size_)
         unlocked_write_to_file();

      message_ = std::ostringstream{ };
   }

   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::put(const char_type c)
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_put(c);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_put(const char_type c)
{
   message_.put(c);
}

template <typename CharT, typename Traits, typename Allocator>
basic_log<CharT, Traits, Allocator>& basic_log<CharT, Traits, Allocator>::write(const gsl::cstring_span<>& s)
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_write(s);
   check_flush();
   return *this;
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::unlocked_write(const gsl::cstring_span<>& s)
{
   message_.write(s.data(), s.length());
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::write_to_file()
{
   auto lock = mutex_lock{ mutex_ };
   unlocked_write_to_file();
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::write_to_file()
{
   std::ofstream o{ filename_, std::ios::app };
   for (const auto& m : buffered_messages_)
      o << m;
   o.close();
   buffered_messages_.clear();
}

template <typename CharT, typename Traits, typename Allocator>
void basic_log<CharT, Traits, Allocator>::check_flush()
{
   if (size_ >= buffer_size_)
      unlocked_flush();
}

template <typename CharT, typename Traits, typename Allocator>
void config(basic_log<CharT, Traits, Allocator>& log)
{
   log << "config: ";
}

template <typename CharT, typename Traits, typename Allocator>
void datetime(basic_log<CharT, Traits, Allocator>& log)
{
   constexpr auto epoch{ 1900 };
   auto current_time{ std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) };
   auto now{ std::localtime(&current_time) };

   log << std::setfill('0')
       << std::setw(2) << now->tm_hour
       << std::setw(2) << now->tm_min
       << std::setw(2) << now->tm_sec << '-'
       << std::setw(2) << now->tm_year + year_offset << '-'
       << std::setw(2) << now->tm_mon  + 1 << '-'
       << std::setw(2) << now->tm_mday + 1;
}

template <typename CharT, typename Traits, typename Allocator>
void error(basic_log<CharT, Traits, Allocator>& log)
{
   log << "error: ";
}

template <typename CharT, typename Traits, typename Allocator>
void event(basic_log<CharT, Traits, Allocator>& log)
{
   log << "event: ";
}

template <typename CharT, typename Traits, typename Allocator>
void internal(basic_log<CharT, Traits, Allocator>& log)
{
   log << "internal: ";
}

template <typename CharT, typename Traits, typename Allocator>
void load(basic_log<CharT, Traits, Allocator>& log)
{
   log << "load: ";
}

template <typename CharT, typename Traits, typename Allocator>
void unload(basic_log<CharT, Traits, Allocator>& log)
{
   log << "unload: ";
}

template <typename CharT, typename Traits, typename Allocator>
void warning(basic_log<CharT, Traits, Allocator>& log)
{
   log << "warning: ";
}
} // namespace doge
#endif // DOGE_LOG_TEM_INCLUDED