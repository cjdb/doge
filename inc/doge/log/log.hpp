#ifndef DOGE_LOG_HPP_INCLUDED
#define DOGE_LOG_HPP_INCLUDED
#include <chrono>
#include <ctime>
#include <fstream>
#include <functional>
#include <gsl/gsl.h>
#include <iomanip>
#include <mutex>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace doge {
template <typename CharT,
          typename Traits = std::char_traits<CharT>,
          typename Allocator = std::allocator<CharT>>
class basic_log
{
public:
   using buffer_type = std::basic_ostringstream<CharT, Traits, Allocator>;
   using string_type = std::basic_string<CharT, Traits, Allocator>;
   using log_t = basic_log<CharT, Traits, Allocator>;

   using char_type = CharT;
   using traits_type = Traits;
   using allocator_type = Allocator;
   using int_type = typename traits_type::int_type;
   using pos_type = typename traits_type::pos_type;
   using off_type = typename traits_type::off_type;

   inline explicit basic_log(bool debug = false);
   inline ~basic_log();

   basic_log(const log_t&) = delete;
   basic_log& operator=(const log_t&) = delete;

   inline basic_log(log_t&&) = default;
   inline basic_log& operator=(log_t&&) = default;

   inline log_t& operator<<(std::basic_ostream<CharT, Traits>& (*pf)(std::basic_ostream<CharT, Traits>&));
   inline log_t& operator<<(std::ios& (*pf)(std::ios&));
   inline log_t& operator<<(std::ios_base& (*pf)(std::ios_base&));
   inline log_t& operator<<(const std::function<void(log_t)>& f);
   inline log_t& operator<<(std::function<void(log_t)>&& f);

   template <typename T>
   inline log_t& operator<<(const T& o);

   inline log_t& flush();
   inline log_t& put(char_type c);
   inline log_t& write(const gsl::cstring_span<>& s);

   inline pos_type tellp();
   inline pos_type seekp();
private:
   using mutex_lock = std::lock_guard<std::mutex>;
   std::size_t buffer_size_;
   buffer_type message_;
   string_type filename_;
   std::vector<string_type> buffered_messages_;
   std::function<void(log_t)> message_type_;
   std::mutex mutex_;

   inline void check_flush();
   inline log_t& unlocked_flush();
   inline void unlocked_write(const gsl::cstring_span<>& s);

   inline log_t& unlocked_put(std::basic_ostream<CharT, Traits>& (*pf)(std::basic_ostream<CharT, Traits>&));
   inline log_t& unlocked_put(std::ios& (*pf)(std::ios&));
   inline log_t& unlocked_put(std::ios_base& (*pf)(std::ios_base&));
   inline log_t& unlocked_put(const std::function<void(log_t)>& f);
   inline log_t& unlocked_put(std::function<void(log_t)>&& f);

   template <typename T>
   inline log_t& unlocked_put(const T& o);
};

template <typename CharT, typename Traits, typename Allocator>
inline void config(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void datetime(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void error(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void event(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void internal(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void load(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void unload(basic_log<CharT, Traits, Allocator>& log);

template <typename CharT, typename Traits, typename Allocator>
inline void warning(basic_log<CharT, Traits, Allocator>& log);

extern basic_log<char> log;
extern basic_log<wchar_t> wlog;
extern basic_log<char16_t> u16log;
extern basic_log<char32_t> u32log;
} // namespace doge
#include "log.tem"
#endif // DOGE_LOG_HPP_INCLUDED