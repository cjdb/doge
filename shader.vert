#version 430

layout (binding = 0) in vec3 vertex_position;
layout (binding = 1) in vec2 vt;

uniform mat4 cam_view, cam_proj, sprite_matrix;

uniform vec2 sprite_sheet;
uniform int ss;

out vec2 texture_coordinates;

void main()
{
   vec4 last;
   if (ss == 0)
   {
      texture_coordinates = vt;
      last = vec4(vertex_position, 1.0);
   }
   else
   {
      texture_coordinates = vt;//(vt + 1.0) * 0.5;
      texture_coordinates = vec2(texture_coordinates.s / 2.0 + sprite_sheet.s,
                                 texture_coordinates.t / 2.0 + sprite_sheet.t);
      last = vec4(vertex_position.x, -1.0, -vertex_position.y, 1.0);
   }

   gl_Position = cam_proj * cam_view * sprite_matrix * vec4(vertex_position, 1.0);
}

