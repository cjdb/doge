#version 330 core

in vec2 st;

uniform sampler2D t1;
uniform sampler2D t2;

out vec4 frag_colour;

void main()
{
   vec4 texel1 = texture(t1, st);
   vec4 texel2 = texture(t2, st);
   frag_colour = mix(texel1, texel2, st.s);
}
