#version 330 core

// fragment stuff
in vec4 fc;
in vec2 st;

// texture stuff
uniform sampler2D tex;

// output
out vec4 frag_colour;

void main()
{
   vec4 texel = texture(tex, st);
   frag_colour = texel * fc;
}
