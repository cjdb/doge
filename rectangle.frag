#version 430 core
in vec4 out_colour;
in vec2 out_tex;
out vec4 colour;

layout (binding = 0) uniform sampler2D tex;

void main()
{
   colour = texture(tex, out_tex);
}
