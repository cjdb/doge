#version 430 core
layout (location = 0) in vec2 position;
layout (location = 1) in vec4 colour;
layout (location = 2) in vec2 tex;

out vec4 out_colour;
out vec2 out_tex;

void main()
{
   gl_Position = vec4(position, 0.0, 1.0);
   out_colour = colour;
   out_tex = tex;
}
