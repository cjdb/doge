// First OpenGL 4 tutorial
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <array>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <doge/entity/camera.hpp>
#include <doge/utility/graphics.hpp>
#include <doge/hid/keyboard.hpp>
#include <doge/hid/mouse.hpp> 
#include <doge/utility/math.hpp>
#include <doge/graphics/shader.hpp>
#include <doge/graphics/sprite.hpp>
#include <doge/graphics/texture.hpp>
#include <doge/graphics/vertex_buffer.hpp>

bool init();
bool create_shader(const std::string& filename, std::uint32_t& shader, const GLenum type);
bool create_program(const std::uint32_t vs, const std::uint32_t fs, std::uint32_t& program);
void print_version_info();
void glfw_window_size_callback(GLFWwindow*, int, int);

int screen_width = 1024;
int screen_height = 1024;

GLFWwindow* window = nullptr;
GLFWmonitor* monitor = nullptr;
const GLFWvidmode* vidmode = nullptr;

int main()
{
   int error_code = 0;
   // start the gl context and OS window using glfw3
   if (init())
   {
      // only draw pixels if the shape is close to the viewer
      //glEnable(GL_DEPTH_TEST); // enables depth testing
      glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as closer

      doge::graphics::shader static_vs{ "static.vert", doge::graphics::shader::vertex };
      doge::graphics::shader static_fs{ "static.frag", doge::graphics::shader::fragment };
      doge::graphics::shader_program static_shader{{{ static_vs, static_fs }}};

      std::vector<GLfloat> f
      {{
         // vertex            colour                     texture
          1.0f,  1.0f,        1.0f, 1.0f, 1.0f, 1.0f,    1.0f, 1.0f,
          1.0f, -1.0f,        1.0f, 1.0f, 1.0f, 1.0f,    1.0f, 0.0f,
         -1.0f, -1.0f,        1.0f, 1.0f, 1.0f, 1.0f,    0.0f, 0.0f,
         -1.0f,  1.0f,        0.4f, 0.5f, 0.0f, 0.9f,    0.0f, 1.0f
      }};
      std::vector<GLuint> e{{ 0, 1, 3, 1, 2, 3 }};
      std::vector<doge::graphics::buffer_data> b{{{ 2, 8, 0 }, { 4, 8, 2 }, { 2, 8, 6 }}};

      
      doge::graphics::texture wall{ "wall.png", static_shader, "tex" };

      std::vector<doge::graphics::sprite> spr(71, { static_shader, f, e, b });
      /*spr.clear();
      for (int i = 0; i < 28; ++i)
         spr.emplace_back(static_shader, f, e, b);*/

      float spr_size = 1.0f / 16.0f;
      glm::vec4 v{ 0, (1.0f - spr_size)/spr_size, 0.0f, 0.0f };
      for (auto i = spr.begin(); i != spr.end(); ++i)
         i->scale(spr_size);
      for (std::size_t i = 0; i != 16; ++i)
      {
         v.x = -(1.0f - i/8.0f - spr_size)/spr_size;
         spr[i].translate(v);
         v.y = -v.y;
         spr[i + 16].translate(v);
         v.y = -v.y;
      }

      v.x = v.y;
      for (std::size_t i = 32; i != 32+14; ++i)
      {
         v.y = (1.0f - (i - 31)/8.0f - spr_size)/spr_size;
         spr[i].translate(v);
         v.x = -v.x;
         spr[i + 14].translate(v);
         v.x = -v.x;
      }

      v.x = -(1.0f - 9.5f/8.0f - spr_size) / spr_size;
      v.y = 0.0f;
      spr[60].translate(v);
      v.y *= -1.0f;
      spr[61].translate(-v);
      v.y = -(1.0f - 8.5f/8.0f - spr_size)/spr_size;
      spr[62].translate(v);
      v.y *= -1.0f;
      spr[63].translate(-v);
      v.y = -(1.0f - 9.5f/8.0f - spr_size)/spr_size;
      spr[64].translate(v);
      v.y *= -1.0f;
      spr[65].translate(-v);
      v.x = -(1.0f - 8.5f / 8.0f - spr_size)/spr_size;
      spr[66].translate(-v);
      v.y *= -1.0f;
      spr[67].translate(v);
      v.x = 0.0f;
      spr[68].translate(v);
      v.x = -(1.0f - 8.5f / 8.0f - spr_size)/spr_size;
      v.y = 0.0f;
      spr[69].translate(v);
      spr[70].translate(-v);
      //glEnable(GL_CULL_FACE);
      //glCullFace(GL_BACK);
      //glFrontFace(GL_CW);
      //float speed = 1.0f;
   
      doge::hid::mouse::init(window);
      doge::hid::keyboard::init(window);
      //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      while (glfwWindowShouldClose(window) == false)
      {
         /*/static double ps = glfwGetTime();
         //double cs = glfwGetTime();
         //double es = cs - ps;
         //ps = cs;

         // update other events
         glfwPollEvents();
         doge::hid::mouse::poll();

         doge::hid::keyboard::on_key_press(GLFW_KEY_ESCAPE, [&](){ glfwSetWindowShouldClose(window, 1); });
         doge::hid::keyboard::on_key_down('W', [&c, speed, es]{ c.move({ 0.0f, 0.0f, -speed * es, 0.0f }); });
         doge::hid::keyboard::on_key_down('A', [&c, speed, es]{ c.move({ -speed * es, 0.0f, 0.0f, 0.0f }); });
         doge::hid::keyboard::on_key_down('S', [&c, speed, es]{ c.move({ 0.0f, 0.0f, speed * es, 0.0f }); });
         doge::hid::keyboard::on_key_down('D', [&c, speed, es]{ c.move({ speed * es, 0.0f, 0.0f, 0.0f }); });
         doge::hid::keyboard::on_key_down(GLFW_KEY_UP, [&c, es]{ c.pitch(30.0f * es); });
         doge::hid::keyboard::on_key_down(GLFW_KEY_DOWN, [&c, es]{ c.pitch(-30.0f * es); });
         doge::hid::keyboard::on_key_down(GLFW_KEY_LEFT, [&c, es]{ c.yaw(30.0f * es); });
         doge::hid::keyboard::on_key_down(GLFW_KEY_RIGHT, [&c, es]{ c.yaw(-30.0f * es); });
         doge::hid::keyboard::on_key_down('Q', [&c, es]{ c.roll(-30.0f * es); });
         doge::hid::keyboard::on_key_down('E', [&c, es]{ c.roll(30.0f * es); });

         // clear screen
         doge::clear_colour(0xccccccff);
         glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
         glViewport(0, 0, screen_width, screen_height);

         glUseProgram(static_shader);
         auto e = vao.enable();
         glDrawElements(GL_TRIANGLES, pe.size(), GL_UNSIGNED_INT, 0);
         // Put the stuff we've been drawing on the display
         glfwSwapBuffers(window);*/
         
         glfwPollEvents();
         doge::hid::keyboard::on_key_press(GLFW_KEY_ESCAPE, [&](){ glfwSetWindowShouldClose(window, 1); });
         doge::clear_colour(0xff00ffff);
         glClear(GL_COLOR_BUFFER_BIT);
         //glUseProgram(static_shader);
         {
            //auto e = vao.enable();
            //glDrawElements(GL_TRIANGLES, pe.size(), GL_UNSIGNED_INT, 0);
            for (auto it = spr.begin(); it != spr.end(); ++it)
               it->draw(wall);
            
         }
         glfwSwapBuffers(window);
      }
   }
   else
   {
      std::cerr << "ERROR: could not start glfw3\n";
      return 1;
   }

   glfwTerminate();
   return error_code;
}

bool init()
{
   if (glfwInit())
   {
      glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
      glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
      glfwWindowHint(GLFW_SAMPLES, 4);

      //monitor = glfwGetPrimaryMonitor();
      //vidmode = glfwGetVideoMode(monitor);
      //window = glfwCreateWindow(vidmode->width, vidmode->height, "Extended GL Init", monitor, nullptr);

      window = glfwCreateWindow(screen_width, screen_height, "doge experiment", nullptr, nullptr);
      if (window)
      {
         glfwSetWindowSizeCallback(window, glfw_window_size_callback);
         glfwMakeContextCurrent(window);
         glewExperimental = GL_TRUE;
         glewInit();
         print_version_info();
         return true;
      }
      else
      {
         std::cerr << "ERROR: could not open window with glfw3" << std::endl;
         return false;
      }
   }
   else
   {
      std::cerr << "ERROR: could not start glfw3" << std::endl;
      return false;
   }
}

bool create_shader(const std::string& path, std::uint32_t& shader, const GLenum type)
{
   // load the shader into memory
   std::ifstream file{ path };
   if (file.bad())
      throw std::runtime_error{ "ERROR: couldn't open shader!\n" };
   std::string text;
   while (file.eof() == false)
      text += file.get();
text.pop_back();
   // create the shader
   const auto* shader_code = text.c_str();
   shader = glCreateShader(type);
   glShaderSource(shader, 1, &shader_code, nullptr);
   glCompileShader(shader);

std::int32_t is_compiled = 0;
glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
if (is_compiled == false)
{
   auto max_length = 0;
   glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &max_length);

   std::vector<char> error_log(max_length);
   glGetShaderInfoLog(shader, max_length, &max_length, &error_log[0]);
   glDeleteShader(shader);
   std::string errmsg{ error_log.cbegin(), error_log.cend() };
   std::cerr << errmsg << std::endl;
   return false;
}

   // check for compile errors
   int params = -1;
   glGetShaderiv(shader, GL_COMPILE_STATUS, &params);
   if (params != GL_TRUE)
   {
      std::cerr << "ERROR: GL shader index " << shader << " did not compile" << std::endl;
      std::cerr << text << std::endl;
      return false;
   }
   else
   {
      std::cout << "Shader " << shader << " compiled successfully.\n";
      return true;
   }

}

bool create_program(const std::uint32_t vs, const std::uint32_t fs, std::uint32_t& program)
{
   program = glCreateProgram();
   std::cout << "Created program " << program << ". Attaching shaders " << vs << " and " << fs << "...\n";
   glAttachShader(program, vs);
   glAttachShader(program, fs);
   glLinkProgram(program);

   // check linking is all good
   std::int32_t params;
   glGetProgramiv(program, GL_LINK_STATUS, &params);
   if (params == GL_TRUE)
   {
      glDeleteShader(vs);
      glDeleteShader(fs);
      return true;
   }
   else
   {
      std::cerr << "ERROR: could not link shader program GL index " << program << '\n';
      return false;
   }
}

void print_version_info()
{
   const auto* renderer = glGetString(GL_RENDERER);
   const auto* version = glGetString(GL_VERSION);
   std::cout << "Renderer: " << renderer << '\n';
   std::cout << "Version: " << version << '\n';
}

void glfw_window_size_callback(GLFWwindow*, int w, int h)
{
   screen_width = w;
   screen_height = h;
}