#version 430

in vec2 texture_coordinates;
uniform sampler2D tex;
uniform sampler2D ship;
out vec4 frag_colour;

void main()
{
   vec4 tex_skull = texture(tex, texture_coordinates);
   vec4 tex_ship = texture(ship, texture_coordinates);
   frag_colour = mix(tex_skull, tex_ship, texture_coordinates.s);
}
