#version 430

in vec2 texture_coordinates;
layout (binding = 0) uniform sampler2D tex;
layout (binding = 1) uniform sampler2D ship;
uniform int mixer;
out vec4 frag_colour;

void main()
{
   if (mixer == 0)
   {
      vec4 texel = texture(tex, texture_coordinates);
      //if (texel.r < 0.01 && texel.g < 0.01 && texel.b < 0.01)
      //   discard;
      frag_colour = texel;
   }
   else
   {
      vec4 tex_skull = texture(tex, texture_coordinates);
      vec4 tex_ship = texture(ship, texture_coordinates);
      frag_colour = mix(tex_skull, tex_ship, texture_coordinates.s);
   }
}

