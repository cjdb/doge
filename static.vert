#version 330 core

layout (location = 0) in vec2 vp;
layout (location = 1) in vec4 vc;
layout (location = 2) in vec2 vt;

// camera stuff
uniform mat4 cam_view;
uniform mat4 cam_proj;

// sprite stuff
uniform mat4 matrix;

// fragment stuff
out vec4 fc;
out vec2 st;

void main()
{
   fc = vc;
   st = vt;
   gl_Position = matrix * vec4(vp, 0.0, 1.0);
}
