#version 330 core

layout (location = 0) in vec3 vp;
layout (location = 1) in vec3 vc;
layout (location = 2) in vec2 vt;

// camera stuff
uniform mat4 cam_view;
uniform mat4 cam_proj;

// sprite stuff
uniform mat4 matrix;
uniform vec2 sheet;

// fragment stuff
out vec2 st;

void main()
{
   st = vt;
   st = vec2(st.s / 2.0 + sheet.s, st.t / 2.0 + sheet.t);
   gl_Position = cam_proj * cam_view * matrix * vec4(vp, 1.0);
}
