#include <cassert>
#include <doge/time/clock.hpp>
#include <doge/utility/units.hpp>
#include <iostream>

using namespace std::literals::chrono_literals;

int main()
{
   doge::time::clock c;

   assert(c.paused() == false);
   assert(c.speed() == 1);
   assert(c.time_since_epoch() < 3_us);
   assert(c.floating_time_since_epoch() < 5000.0);

   c.set_speed(1'000'000'000);
   assert(c.speed() == 1'000'000'000);
   c.reset();
   assert(1s < c.time_since_epoch());
   return 0;
}
