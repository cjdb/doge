#include <doge/time/clock.hpp>
#include <doge/time/timer.hpp>
#include <iostream>

doge::time::clock c;
int main()
{
   doge::time::timer t{ 10_s, c };
   t.deep_sleep();
   std::cout << "done" << std::endl;

   t.reset(20_s);
   auto n = c.time_since_epoch();
   std::size_t s = 0;
   while (t.expired() == false)
   {
      if (c.time_since_epoch() - n >= 1_s)
      {
         ++s;
         std::cout << s << std::endl;
         n = c.time_since_epoch();
      }
   }
   std::cout << "done." << std::endl;
}