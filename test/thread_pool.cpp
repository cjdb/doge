#include <atomic>
#include <cassert>
#include <chrono>
#include <condition_variable>
#include <doge/system/thread_pool.hpp>
#include <iostream>
#include <mutex>
#include <random>
#include <sstream>
#include <vector>

void fleeting();
void fast(std::size_t);
void medium(std::size_t);
void slow(std::size_t);
void choose_async();
void end();

template <std::size_t bitshift>
void stress_test()
{
   constexpr std::size_t max = std::size_t(1) << bitshift;
   for (std::size_t i = 0; i < max; ++i);

   choose_async();
   end();
}

std::vector<std::string> v;
std::mutex mutex;
std::condition_variable cv;
std::size_t i = 0;
doge::system::thread_pool t;
constexpr std::size_t max_ops = 10'000;
std::mt19937_64 generator(std::chrono::system_clock::now().time_since_epoch().count());
std::uniform_int_distribution<std::size_t> dist{ 0, max_ops };

std::atomic_size_t fleeting_range;
std::atomic_size_t fast_range;
std::atomic_size_t medium_range;

int main(int argc, char* argv[])
{
   assert(argc == 4);

   double fleeting_chance, fast_chance, medium_chance;
   std::istringstream i{ argv[1] };
   i >> fleeting_chance;

   i = std::istringstream{ argv[2] };
   i >> fast_chance;

   i = std::istringstream{ argv[3] };
   i >> medium_chance;

   std::cout << fleeting_chance << ' ' << fast_chance << ' ' << medium_chance << std::endl;

   assert(static_cast<std::size_t>(fleeting_chance + fast_chance + medium_chance) <= 1);

   fleeting_range = std::ceil(max_ops * fleeting_chance);
   fast_range     = fleeting_range + std::ceil(max_ops * fast_chance);
   medium_range   = fast_range + std::ceil(max_ops * medium_chance);

   assert(medium_range <= max_ops);

   for (int i = 0; i < 12; ++i)
      choose_async();

   std::unique_lock<std::mutex> lk{ mutex };
   cv.wait(lk, []{ return t.finished(); });
   std::cout << "Game over." << std::endl;
   return 0;
}

void choose_async()
{
   std::size_t p;
   {
      std::lock_guard<std::mutex> lock{ mutex };
      p = dist(generator);
   }
   
   if (p <= fleeting_range)
      t.add_task(fleeting);
   else if (p <= fast_range)
      t.add_task(stress_test<15>);
   else if (p <= medium_range)
      t.add_task(stress_test<20>);
   else
      t.add_task(stress_test<38>);
}

void end()
{
   std::lock_guard<std::mutex> lock{ mutex };
   ++i;
   if (i == max_ops)
   {
      std::cout << "Finishing..." << std::endl;
      t.finish();
      cv.notify_one();
   }
}

void fleeting()
{
   std::size_t k = 0;
   ++k;
   choose_async();
   end();
}
/*
void fast(std::size_t k)
{
   constexpr std::size_t bitshift = 16;
   for (std::size_t j = 0; j < (std::size_t(1) << bitshift); ++j)
       ++k;

   choose_async(k);
   end();
}

void medium(std::size_t k)
{
   for (std::size_t i = 0; i < (std::size_t(1) << 32); ++i, ++k);
   //for (std::uint32_t i = 0; i < std::uint32_t(-1); ++i, ++k);
   choose_async(k);
   end();
}

void slow(std::size_t k)
{
   for (std::size_t i = 0; i < (std::size_t(1) << 37); ++i, ++k);
   choose_async(k);
   end();
}
*/
