#include <cassert>
#include <doge/system/error_codes.hpp>
#include <doge/system/memory.hpp>
#include <doge/system/memory_list.hpp>
#include <doge/utility/units.hpp>
#include <iomanip>
#include <iostream>

using namespace doge::system;
namespace doge {
namespace system {
const memory_list& get_list();
}
}

int main()
{
   try
   {
      doge::system::memory m{ 6_B };
   }
   catch (doge::system::error_code e)
   {
      if (e == doge::system::error_code::bad_allocation_size)
         std::cerr << "Bad allocation size... test passed!" << std::endl;
      else
         throw;
   }

   try
   {
      const auto& free_list = get_list();
      std::cerr << "Constructing memory allocator for test 1... ";
      doge::system::memory m{ 512_B };
      std::cerr << "done!" << std::endl;

      std::cerr << "int* i = new int... ";
      int* i = new int;
      std::cerr << "done!" << std::endl;
      std::cerr << "int* j = new int... "; 
      int* j = new int;
      std::cerr << "done!" << std::endl;
      int* k = new int;

      /*std::cerr << "i == " << i << std::endl;
      std::cerr << "j == " << j << std::endl;
      std::cerr << "(j - i) == " << (j - i) << std::endl;
      std::cerr << "(j - i) * sizeof(int*) == " << (j - i) * sizeof(int) << std::endl;
      std::cerr << "sizeof(node) == " << sizeof(node) << std::endl;
      std::cerr << "sizeof(node) + sizeof(int) == " << (sizeof(node) + sizeof(int)) << std::endl;*/

      std::cerr << std::boolalpha;
      std::cerr << "i != j... " << (i != j) << std::endl;
      std::cerr << "j - i == sizeof(node) + sizeof(int)... " << (((char*)j - (char*)i) == sizeof(doge::system::node) + sizeof(int)) << std::endl;

      std::cerr << "delete i... ";
      delete i;
      std::cerr << "done!" << std::endl;
      std::cerr << "free_list.head_ == free_list.head_->next... " << (free_list.head_->next == free_list.head_) << std::endl;

      std::cerr << "delete j... ";
      delete j;
      std::cerr << "done!" << std::endl;
      std::cerr << "free_list.head_ == free_list.head_->next... " << (free_list.head_ == free_list.head_->next) << std::endl;
      delete k;
   }
   catch (doge::system::error_code e)
   {
      if (e == doge::system::error_code::bad_allocation_size)
         std::cerr << "Bad allocation size" << std::endl;
   }
   return 0;
}
