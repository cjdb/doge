#version 430 core

in vec2 tex;
out vec4 colour;

layout (binding = 0) uniform sampler2D tex_diffuse;

void main()
{
   colour = vec4(texture(tex_diffuse, tex));
}

