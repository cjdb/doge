/**
 * Christopher Di Bella
 * cjdb.ns@gmail.com
 *
 * 2015/12/23
 *
 * Demo file for camera
 *
 *  Copyright 2015 Christopher Di Bella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gl/gl_core_4_3.hpp>
#include <doge/engine.hpp>
#include <doge/entity/camera.hpp>
#include <doge/graphics/element_array.hpp>
#include <doge/graphics/model.hpp>
#include <doge/graphics/shader.hpp>
#include <doge/graphics/shader_program.hpp>
#include <doge/graphics/vertex.hpp>
#include <doge/graphics/vertex_buffer.hpp>
#include <doge/game.hpp>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <stb_image.h>

using doge::graphics::shader;
namespace graphics = doge::graphics;
namespace keyboard = doge::hid::keyboard;
namespace mouse = doge::hid::mouse;

class camera_demo : public doge::game
{
   doge::entity::camera _c;
   std::vector<doge::graphics::model> _models;
   graphics::shader_program _shader;

   GLuint vao;
   std::vector<graphics::sprite_vertex> v;
   graphics::sprite_buffer vbo;
   graphics::element_array eao;
   graphics::texture tex;

   /*GLint _model;
   GLint _view;
   GLint _projection;*/

   void print(const glm::mat4& model)
   {
      std::cout << model[0][0] << '\t' << model[0][1] << '\t' << model[0][2] << '\t' << model[0][3] << '\n';
      std::cout << model[1][0] << '\t' << model[1][1] << '\t' << model[1][2] << '\t' << model[1][3] << '\n';
      std::cout << model[2][0] << '\t' << model[2][1] << '\t' << model[2][2] << '\t' << model[2][3] << '\n';
      std::cout << model[3][0] << '\t' << model[3][1] << '\t' << model[3][2] << '\t' << model[3][3] << std::endl;
   }
public:
   camera_demo()
      : _c{{ 0.0f, 0.0f, 3.0f }},
        _shader{{ shader{ "rectangle.vert", shader::vertex }, shader{ "rectangle.frag", shader::fragment } }},
        vao{ 0 },
        v{ graphics::sprite_vertex{ glm::vec2{  1.0f,  1.0f }, glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f }, glm::vec2{ 1.0f, 1.0f } },
           graphics::sprite_vertex{ glm::vec2{  1.0f, -1.0f }, glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f }, glm::vec2{ 1.0f, 0.0f } },
           graphics::sprite_vertex{ glm::vec2{ -1.0f, -1.0f }, glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f }, glm::vec2{ 0.0f, 0.0f } },
           graphics::sprite_vertex{ glm::vec2{ -1.0f,  1.0f }, glm::vec4{ 0.4f, 0.5f, 0.0f, 0.9f }, glm::vec2{ 0.0f, 1.0f } }
         },
         vbo{ v },
         eao{std::vector<GLuint>{ 0, 1, 3, 1, 2, 3 }},
         tex{ "Untitled.png", _shader }
/*        _model{ _shader.get_uniform("model") },
        _view{ _shader.get_uniform("view") },
        _projection{ _shader.get_uniform("projection") }*/
   {
      //_models.emplace_back("test/common/nanosuit/nanosuit2.obj", _shader);
      /*std::vector<graphics::sprite_vertex> f{ { {  1.0f,  1.0f },        { 1.0f, 1.0f, 1.0f, 1.0f },    { 1.0f, 1.0f } },
                                    { {  1.0f, -1.0f },        { 1.0f, 1.0f, 1.0f, 1.0f },    { 1.0f, 0.0f } },
                                    { { -1.0f, -1.0f },        { 1.0f, 1.0f, 1.0f, 1.0f },    { 0.0f, 0.0f } },
                                    { { -1.0f,  1.0f },        { 0.4f, 0.5f, 0.0f, 0.9f },    { 0.0f, 1.0f } }
                                  };
      std::vector<GLuint> e {{ 0, 1, 3, 1, 2, 3 }};
      gl::GenVertexArrays(1, &_vao);
      gl::GenBuffers(1, &_vbo);
      gl::GenBuffers(1, &_ebo);*/
std::cout << eao.size() << std::endl;
      gl::GenVertexArrays(1, &vao);
      gl::BindVertexArray(vao);
      gl::BindBuffer(gl::ARRAY_BUFFER, vbo.vbo);
      gl::BufferData(gl::ARRAY_BUFFER, vbo.size(), vbo.data(), gl::STATIC_DRAW);
      gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, eao.ebo);
      gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, eao.size(), eao.data(), gl::STATIC_DRAW);

      gl::VertexAttribPointer(0, 2, gl::FLOAT, false, vbo.buffer_width(), nullptr);
      gl::EnableVertexAttribArray(0);
      gl::VertexAttribPointer(1, 4, gl::FLOAT, false, vbo.buffer_width(), (void*)(2 * sizeof(GLfloat)));
      gl::EnableVertexAttribArray(1);
      gl::VertexAttribPointer(2, 2, gl::FLOAT, false, vbo.buffer_width(), (void*)(6 * sizeof(GLfloat)));
      gl::EnableVertexAttribArray(2);
      gl::BindVertexArray(0);

/*      gl::EnableVertexAttribArray(graphics::sprite_vertex::position);
      gl::BindVertexBuffer(graphics::sprite_vertex::position, vbo.vbo, 0, vbo.size());
      gl::VertexAttribFormat(graphics::sprite_vertex::position, sizeof(GLfloat) * 2, gl::FLOAT, false, vbo.buffer_width());
      gl::VertexAttribBinding(graphics::sprite_vertex::position, graphics::sprite_vertex::position);

      gl::EnableVertexAttribArray(graphics::sprite_vertex::colour);
      gl::BindVertexBuffer(graphics::sprite_vertex::colour, vbo.vbo, sizeof(GLfloat) * 2, vbo.size());
      gl::VertexAttribFormat(graphics::sprite_vertex::colour, sizeof(GLfloat) * 4, gl::FLOAT, false, vbo.buffer_width());
      gl::VertexAttribBinding(graphics::sprite_vertex::colour, graphics::sprite_vertex::colour);

      gl::EnableVertexAttribArray(graphics::sprite_vertex::texture);
      gl::BindVertexBuffer(graphics::sprite_vertex::texture, vbo.vbo, sizeof(GLfloat) * 6, vbo.size());
      gl::VertexAttribFormat(graphics::sprite_vertex::texture, sizeof(GLfloat) * 2, gl::FLOAT, false, vbo.buffer_width());
      gl::VertexAttribBinding(graphics::sprite_vertex::texture, graphics::sprite_vertex::texture);*/

      std::cout.sync_with_stdio(false);
   }

   void frame() override
   {
      /*static GLfloat last_frame = glfwGetTime();
      const GLfloat current_frame = glfwGetTime();
      const GLfloat delta = current_frame - last_frame;
      last_frame = current_frame;*/

      _shader.activate();

      /*_c.draw();
      _shader.set_uniform(_view, _c.view);
      _shader.set_uniform(_projection, _c.projection);

      std::cout << "===== View =====\n";
      print(_c.view);

      std::cout << "===== Projection =====\n";
      print(_c.projection);

      glm::mat4 model;
      model = glm::translate(model, glm::vec3{ 0.0f, -1.75f, 0.0f });
      model = glm::scale(model, glm::vec3{ 0.2f, 0.2f, 0.2f });
      _shader.set_uniform(_model, model);

      for (const auto& m : _models)
      {
         std::cout << "===== Model =====\n";
         print(model);
         m.draw(_shader);
      }

      glm::vec2 cursor_delta = mouse::cursor_delta();
      _c.yaw(cursor_delta.x);
      _c.pitch(cursor_delta.y);

      keyboard::on_key_down('W', [&]{ _c.move({  0.0f,  0.0f, -1.0f }, delta); });
      keyboard::on_key_down('A', [&]{ _c.move({ -1.0f,  0.0f,  0.0f }, delta); });
      keyboard::on_key_down('S', [&]{ _c.move({  0.0f,  0.0f,  1.0f }, delta); });
      keyboard::on_key_down('D', [&]{ _c.move({  1.0f,  0.0f,  0.0f }, delta); });
      keyboard::on_key_down('Z', [&]{ _c.move({  0.0f,  1.0f,  0.0f }, delta); });
      keyboard::on_key_down('X', [&]{ _c.move({  0.0f, -1.0f,  0.0f }, delta); });*/
      gl::BindVertexArray(vao);
      //tex.draw();
      gl::DrawElements(gl::TRIANGLES, eao.size(), gl::UNSIGNED_INT, 0);

      keyboard::on_key_down(GLFW_KEY_ESCAPE, []{ std::abort(); });
   }

   game* clone() override { return nullptr; }
};

int main(int argc, char* argv[])
{
   doge::engine engine{ argc, argv };
   engine.make_game<camera_demo>();
   engine.run();
   return 0;
}
