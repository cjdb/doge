#include <doge/engine.hpp>
#include <doge/hid/keyboard.hpp>

//template <std::size_t S>
GLFWwindow* doge::hid::human_interface_device::window = nullptr;

int main(int argc, char* argv[])
{
   doge::engine g(argc, argv);
   g.loop();
   return 0;
}